# RAM core microservices infra provisioning and initial deployment

The terrafrom code will:

1. Deploy the infrastructure (e.g. pubsub topics ...)
2. Deploy the initial revision of the Cloud Run services
3. Complete infrastructure deployment by provisioning resources that depends on Cloud Run services existance like EventArc triggers and or Pubsub subscriptions

Steps:

- Clear [terraform module](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor) pre-requisites
- Set enviroment variables. This can be a `set_env_vars.sh` file to be run with `source ./set_env_vars.sh` like this one:

```shell
# $1 is the environment, test or prod. test by default. do not use dev that is reserved for the project hosting docker container images, code unit and integration tests
if [[ -z "$1" ]]; then
    environment="test"
else
    environment=$1
fi
if [ $environment == "test" ] || [ $environment == "prod" ]; then
    if [ $environment == "prod" ]; then
        export PROJECT_ID="our-gcp-project-id-for-production"
    else
        export PROJECT_ID="our-gcp-project-id-for-test"
    fi
    echo PROJECT_ID $PROJECT_ID
    export GOOGLE_APPLICATION_CREDENTIALS="/your/path/to/terraform_service_account_key"
else
    echo "Error - environment must be 'test' or 'prod'"
fi
```

- If your GCP project factory does not enable APIs, the script `enable_apis.sh` may help
- Uncomment code in `main.tf` and adapt settings to hold terraform state on your GCS bucket created in the previous step
- Create `terraform.tfvars` file like the example at the end of this document, replacing the following strings
  - `your-ram-gcp-project-id-in-test`
  - `your-ram-gcp-project-id-in-prod`
  - `your_org_id0123456789`
  - `your_fld_id0123456789`
  - It is possible to target one or multiple organizations or folders. Remove what is not needed if only one organization or one folder.
- Review the `variables.tf` file. Whne default value need to be customized, update your `terraform.tfvars` accordingly, for exemple regions where RAM infra is deployed, job shedulers settings, logging level etc.
- `terraform init`
- `terraform workspace new test` to start deploying in test
- `terraform plan` and review
- Double check the pre-requisite to set Firestore to Native-mode has been cleared. If not deploying convertfeed microservice in the next step will fail on a missing database error.
- `terraform apply`
- Keep track of terraform output:
  - `deploy_service_account_email` to be used in a CICD chain to deploy new RAM versions
  - `fetchrules_rules_repo_bucket_name` to be used to publish the compliance rules
  - `launch_actions_repo_bucket_name` to be used to publish the scheduled action configuration files
- Check provisioning resource fron the console: Cloud run, EventArc, PubSub, BiqQuery, Firestore ...

Done. Your RAM core instance is installed

## terraform.tfvars sample to be adapted

```terraform
test_project_id  = "your-ram-gcp-project-id-in-test"
prod_project_id = "your-ram-gcp-project-id-in-prod"
export_org_ids    = [your_org_id0123456789] #for cloud asset owner roles
export_folder_ids = [your_fld_id0123456789] #for cloud asset owner roles
feed_resource_orgs = {
  your_org_id0123456789 = [
    "appengine.googleapis.com/Application",
    "appengine.googleapis.com/Service",
    "appengine.googleapis.com/Version",
    "bigquery.googleapis.com/Dataset",
    "cloudfunctions.googleapis.com/CloudFunction",
    "cloudkms.googleapis.com/CryptoKey",
    "cloudresourcemanager.googleapis.com/Folder",
    "cloudresourcemanager.googleapis.com/Organization",
    "cloudresourcemanager.googleapis.com/Project",
    "composer.googleapis.com/Environment",
    "compute.googleapis.com/Disk",
    "compute.googleapis.com/Firewall",
    "compute.googleapis.com/Instance",
    "compute.googleapis.com/Network",
    "compute.googleapis.com/Router",
    "compute.googleapis.com/Subnetwork",
    "compute.googleapis.com/TargetHttpsProxy",
    "compute.googleapis.com/TargetSslProxy",
    "container.googleapis.com/Cluster",
    "dns.googleapis.com/ManagedZone",
    "iam.googleapis.com/ServiceAccountKey",
    "sqladmin.googleapis.com/Instance",
    "storage.googleapis.com/Bucket",
  ]
}

feed_iam_policy_orgs = {
  your_org_id0123456789 = [
    "appengine.googleapis.com/Application",
    "appengine.googleapis.com/Service",
    "appengine.googleapis.com/Version",
    "bigquery.googleapis.com/Dataset",
    "bigquery.googleapis.com/Table",
    "bigtableadmin.googleapis.com/Cluster",
    "bigtableadmin.googleapis.com/Instance",
    "bigtableadmin.googleapis.com/Table",
    "cloudbilling.googleapis.com/BillingAccount",
    "cloudfunctions.googleapis.com/CloudFunction",
    "cloudkms.googleapis.com/CryptoKey",
    "cloudkms.googleapis.com/CryptoKeyVersion",
    "cloudkms.googleapis.com/KeyRing",
    "cloudresourcemanager.googleapis.com/Folder",
    "cloudresourcemanager.googleapis.com/Organization",
    "cloudresourcemanager.googleapis.com/Project",
    "composer.googleapis.com/Environment",
    "compute.googleapis.com/Address",
    "compute.googleapis.com/Autoscaler",
    "compute.googleapis.com/BackendBucket",
    "compute.googleapis.com/BackendService",
    "compute.googleapis.com/Disk",
    "compute.googleapis.com/Firewall",
    "compute.googleapis.com/ForwardingRule",
    "compute.googleapis.com/GlobalAddress",
    "compute.googleapis.com/GlobalForwardingRule",
    "compute.googleapis.com/HealthCheck",
    "compute.googleapis.com/HttpHealthCheck",
    "compute.googleapis.com/HttpsHealthCheck",
    "compute.googleapis.com/Image",
    "compute.googleapis.com/Instance",
    "compute.googleapis.com/InstanceGroup",
    "compute.googleapis.com/InstanceGroupManager",
    "compute.googleapis.com/InstanceTemplate",
    "compute.googleapis.com/Interconnect",
    "compute.googleapis.com/InterconnectAttachment",
    "compute.googleapis.com/License",
    "compute.googleapis.com/Network",
    "compute.googleapis.com/Project",
    "compute.googleapis.com/RegionBackendService",
    "compute.googleapis.com/RegionDisk",
    "compute.googleapis.com/Route",
    "compute.googleapis.com/Router",
    "compute.googleapis.com/SecurityPolicy",
    "compute.googleapis.com/Snapshot",
    "compute.googleapis.com/SslCertificate",
    "compute.googleapis.com/Subnetwork",
    "compute.googleapis.com/TargetHttpProxy",
    "compute.googleapis.com/TargetHttpsProxy",
    "compute.googleapis.com/TargetInstance",
    "compute.googleapis.com/TargetPool",
    "compute.googleapis.com/TargetSslProxy",
    "compute.googleapis.com/TargetTcpProxy",
    "compute.googleapis.com/TargetVpnGateway",
    "compute.googleapis.com/UrlMap",
    "compute.googleapis.com/VpnTunnel",
    "container.googleapis.com/Cluster",
    "container.googleapis.com/NodePool",
    "datafusion.googleapis.com/Instance",
    "dataproc.googleapis.com/Cluster",
    "dataproc.googleapis.com/Job",
    "dns.googleapis.com/ManagedZone",
    "dns.googleapis.com/Policy",
    "extensions.k8s.io/Ingress",
    "iam.googleapis.com/Role",
    "iam.googleapis.com/ServiceAccount",
    "iam.googleapis.com/ServiceAccountKey",
    "k8s.io/Namespace",
    "k8s.io/Node",
    "k8s.io/Pod",
    "k8s.io/Service",
    "networking.k8s.io/Ingress",
    "pubsub.googleapis.com/Subscription",
    "pubsub.googleapis.com/Topic",
    "rbac.authorization.k8s.io/ClusterRole",
    "rbac.authorization.k8s.io/ClusterRoleBinding",
    "rbac.authorization.k8s.io/Role",
    "rbac.authorization.k8s.io/RoleBinding",
    "serviceusage.googleapis.com/Service",
    "spanner.googleapis.com/Database",
    "spanner.googleapis.com/Instance",
    "sqladmin.googleapis.com/Instance",
    "storage.googleapis.com/Bucket",
  ]
}

feed_iam_policy_folders = {
  your_fld_id0123456789 = [
    "appengine.googleapis.com/Application",
    "appengine.googleapis.com/Service",
    "appengine.googleapis.com/Version",
    "bigquery.googleapis.com/Dataset",
    "bigquery.googleapis.com/Table",
    "bigtableadmin.googleapis.com/Cluster",
    "bigtableadmin.googleapis.com/Instance",
    "bigtableadmin.googleapis.com/Table",
    "cloudbilling.googleapis.com/BillingAccount",
    "cloudfunctions.googleapis.com/CloudFunction",
    "cloudkms.googleapis.com/CryptoKey",
    "cloudkms.googleapis.com/CryptoKeyVersion",
    "cloudkms.googleapis.com/KeyRing",
    "cloudresourcemanager.googleapis.com/Folder",
    "cloudresourcemanager.googleapis.com/Organization",
    "cloudresourcemanager.googleapis.com/Project",
    "composer.googleapis.com/Environment",
    "compute.googleapis.com/Address",
    "compute.googleapis.com/Autoscaler",
    "compute.googleapis.com/BackendBucket",
    "compute.googleapis.com/BackendService",
    "compute.googleapis.com/Disk",
    "compute.googleapis.com/Firewall",
    "compute.googleapis.com/ForwardingRule",
    "compute.googleapis.com/GlobalAddress",
    "compute.googleapis.com/GlobalForwardingRule",
    "compute.googleapis.com/HealthCheck",
    "compute.googleapis.com/HttpHealthCheck",
    "compute.googleapis.com/HttpsHealthCheck",
    "compute.googleapis.com/Image",
    "compute.googleapis.com/Instance",
    "compute.googleapis.com/InstanceGroup",
    "compute.googleapis.com/InstanceGroupManager",
    "compute.googleapis.com/InstanceTemplate",
    "compute.googleapis.com/Interconnect",
    "compute.googleapis.com/InterconnectAttachment",
    "compute.googleapis.com/License",
    "compute.googleapis.com/Network",
    "compute.googleapis.com/Project",
    "compute.googleapis.com/RegionBackendService",
    "compute.googleapis.com/RegionDisk",
    "compute.googleapis.com/Route",
    "compute.googleapis.com/Router",
    "compute.googleapis.com/SecurityPolicy",
    "compute.googleapis.com/Snapshot",
    "compute.googleapis.com/SslCertificate",
    "compute.googleapis.com/Subnetwork",
    "compute.googleapis.com/TargetHttpProxy",
    "compute.googleapis.com/TargetHttpsProxy",
    "compute.googleapis.com/TargetInstance",
    "compute.googleapis.com/TargetPool",
    "compute.googleapis.com/TargetSslProxy",
    "compute.googleapis.com/TargetTcpProxy",
    "compute.googleapis.com/TargetVpnGateway",
    "compute.googleapis.com/UrlMap",
    "compute.googleapis.com/VpnTunnel",
    "container.googleapis.com/Cluster",
    "container.googleapis.com/NodePool",
    "datafusion.googleapis.com/Instance",
    "dataproc.googleapis.com/Cluster",
    "dataproc.googleapis.com/Job",
    "dns.googleapis.com/ManagedZone",
    "dns.googleapis.com/Policy",
    "extensions.k8s.io/Ingress",
    "iam.googleapis.com/Role",
    "iam.googleapis.com/ServiceAccount",
    "iam.googleapis.com/ServiceAccountKey",
    "k8s.io/Namespace",
    "k8s.io/Node",
    "k8s.io/Pod",
    "k8s.io/Service",
    "networking.k8s.io/Ingress",
    "pubsub.googleapis.com/Subscription",
    "pubsub.googleapis.com/Topic",
    "rbac.authorization.k8s.io/ClusterRole",
    "rbac.authorization.k8s.io/ClusterRoleBinding",
    "rbac.authorization.k8s.io/Role",
    "rbac.authorization.k8s.io/RoleBinding",
    "serviceusage.googleapis.com/Service",
    "spanner.googleapis.com/Database",
    "spanner.googleapis.com/Instance",
    "sqladmin.googleapis.com/Instance",
    "storage.googleapis.com/Bucket",
  ]
}

feed_resource_folders = {
  your_fld_id0123456789 = [
    "appengine.googleapis.com/Application",
    "appengine.googleapis.com/Service",
    "appengine.googleapis.com/Version",
    "bigquery.googleapis.com/Dataset",
    "cloudfunctions.googleapis.com/CloudFunction",
    "cloudkms.googleapis.com/CryptoKey",
    "cloudresourcemanager.googleapis.com/Folder",
    "cloudresourcemanager.googleapis.com/Organization",
    "cloudresourcemanager.googleapis.com/Project",
    "composer.googleapis.com/Environment",
    "compute.googleapis.com/Disk",
    "compute.googleapis.com/Firewall",
    "compute.googleapis.com/Instance",
    "compute.googleapis.com/Network",
    "compute.googleapis.com/Router",
    "compute.googleapis.com/Subnetwork",
    "compute.googleapis.com/TargetHttpsProxy",
    "compute.googleapis.com/TargetSslProxy",
    "container.googleapis.com/Cluster",
    "dns.googleapis.com/ManagedZone",
    "iam.googleapis.com/ServiceAccountKey",
    "sqladmin.googleapis.com/Instance",
    "storage.googleapis.com/Bucket",
  ]
}
autofix_org_ids = [your_org_id0123456789]
```
