Feature: assess in realtime rules on asset type bigquery.googleapis.com/Dataset

    As a compliance manager
    I want to assess rules in realtime that are related to a given asset type when an instance of that type is created, updated or deleted
    so that:
    - non compliances could be detected qucikly enabling fact remedation
    - assets compliance status are always fresh

    Scenario: location non compliant BQ dataset are detected on the fly
        Given the ram project is your-ram-project-id
        And the asset or content type is bigquery.googleapis.com/Dataset
        And the rule GCPBigQueryDatasetLocationConstraintV1 with constraint bq_dataset_location_europe is deployed
        And the resource project is your-resource-project-id-for-rule-test
        When a bigquerydataset is created in location us-central1
        And waiting for the latency threshold of 01-ram service ram-e2e-latency-real-time slo
        And the compliance has been analyzed in less than 5 minutes
        And the last asset compliance status to this rule is false
        And the count of active violations is 1
