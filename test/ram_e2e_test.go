// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"cloud.google.com/go/bigquery"
	logadmin "cloud.google.com/go/logging/logadmin"
	monitoring "cloud.google.com/go/monitoring/apiv3"
	scheduler "cloud.google.com/go/scheduler/apiv1"
	"cloud.google.com/go/storage"
	"github.com/cucumber/godog"
	jsoniter "github.com/json-iterator/go"
	"github.com/schollz/progressbar/v3"
	"gitlab.com/realtime-asset-monitor/fetchrules"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
	"google.golang.org/api/cloudresourcemanager/v3"
	"google.golang.org/api/iterator"
	schedulerpb "google.golang.org/genproto/googleapis/cloud/scheduler/v1"
	monitoringpb "google.golang.org/genproto/googleapis/monitoring/v3"
	"google.golang.org/protobuf/types/known/structpb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// the following are of concrete type struct{} as recommended when using context.WithValue
type contentOrAssetT struct{}
type constrName struct{}
type dsName struct{}
type endTimeKey struct{}
type jID struct{}
type jobStartTimeKey struct{}
type loopbackPeriodSecondsKey struct{}
type ramProjectID struct{}
type resourceProjectID struct{}
type rootTriggerTs struct{}
type rTriggerID struct{}
type ruName struct{}
type sID struct{}
type tagValShortName struct{}

func theRAMProjectIs(ctx context.Context, projectID string) (context.Context, error) {
	if projectID == "your-ram-project-id" {
		projectID = os.Getenv("FETCHRULES_PROJECT_ID")
	}
	return context.WithValue(ctx, ramProjectID{}, projectID), nil
}

func theFileHasBeenCopiedToTheGcsBucket(ctx context.Context, actionsFileName string, actionsRepoSuffix string) (context.Context, error) {
	projectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}
	envVarActionsFile := os.Getenv("ACTIONS_FILE")
	if envVarActionsFile != "" {
		actionsFileName = envVarActionsFile
	}

	p := filepath.Clean(fmt.Sprintf("./%s", actionsFileName))
	b, err := os.ReadFile(p)
	if err != nil {
		return ctx, err
	}

	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		return ctx, err
	}
	rulesRepoBucket := storageClient.Bucket(fmt.Sprintf("%s-%s", projectID, actionsRepoSuffix))
	storageObject := rulesRepoBucket.Object(actionsFileName)
	storageObjectWriter := storageObject.NewWriter(ctx)
	_, err = fmt.Fprint(storageObjectWriter, string(b))
	if err != nil {
		return ctx, err
	}
	err = storageObjectWriter.Close()
	if err != nil {
		return ctx, err
	}
	// fmt.Printf("%s file uploaded to gcs bucket %s", settings.ActionsFileName, settings.RulesRepoBucketName)
	return ctx, nil
}

func theJobLocatedInIsManuallyTriggered(ctx context.Context, jobID string, locationID string) (context.Context, error) {
	projectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}

	c, err := scheduler.NewCloudSchedulerClient(ctx)
	if err != nil {
		return ctx, err
	}
	defer c.Close()
	name := fmt.Sprintf("projects/%s/locations/%s/jobs/%s", projectID, locationID, jobID)
	reqRun := &schedulerpb.RunJobRequest{
		Name: name,
	}
	_, err = c.RunJob(ctx, reqRun)
	if err != nil {
		return ctx, err
	}
	// wait 2 sec for job status to be eventually updated before requesting it
	time.Sleep(time.Duration(2 * time.Second))
	reqGet := &schedulerpb.GetJobRequest{
		Name: name,
	}
	job, err := c.GetJob(ctx, reqGet)
	if err != nil {
		return ctx, err
	}
	job.LastAttemptTime.AsTime()
	// fmt.Printf("job %s \njobStartTime: %v\nstatusCode %d\nstatusMessage %s\nstatusDetails %v\n",
	// 	name,
	// 	job.LastAttemptTime.AsTime(),
	// 	job.GetStatus().Code,
	// 	job.GetStatus().Message,
	// 	job.GetStatus().Details)

	ctx1 := context.WithValue(ctx, jID{}, jobID)
	return context.WithValue(ctx1, jobStartTimeKey{}, job.LastAttemptTime.AsTime()), nil
}

func waitingForTheLatencyThresholdOfServiceSlo(ctx context.Context, serviceID string, sloName string) (context.Context, error) {
	projectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}

	c, err := monitoring.NewServiceMonitoringClient(ctx)
	if err != nil {
		return ctx, err
	}

	req := &monitoringpb.GetServiceLevelObjectiveRequest{
		Name: fmt.Sprintf("projects/%s/services/%s/serviceLevelObjectives/%s", projectID, serviceID, sloName),
	}
	resp, err := c.GetServiceLevelObjective(ctx, req)
	if err != nil {
		return ctx, err
	}
	latencyThresholdSeconds := resp.GetServiceLevelIndicator().GetRequestBased().GetDistributionCut().GetRange().GetMax()
	jobStartTime, ok := ctx.Value(jobStartTimeKey{}).(time.Time)
	if !ok {
		return ctx, errors.New("there is no jobStartTime")
	}
	loopbackPeriod := time.Duration(latencyThresholdSeconds * float64(time.Second))
	loopbackPeriodSeconds := int64(loopbackPeriod.Round(time.Second).Seconds())
	ctx1 := context.WithValue(ctx, loopbackPeriodSecondsKey{}, loopbackPeriodSeconds)

	endTime := jobStartTime.Add(loopbackPeriod)
	// slo_burn_rate GAUGE is computed at each minute, drop everything after the minute
	endTimeMinute := time.Date(endTime.Year(), endTime.Month(), endTime.Day(), endTime.Hour(), endTime.Minute(), 0, 0, time.UTC)
	var endTimepb timestamppb.Timestamp
	endTimepb.Seconds = endTimeMinute.Unix()
	endTimepb.Nanos = int32(endTimeMinute.Nanosecond())
	// fmt.Printf("latencyThresholdSeconds %v\nloopbackPeriod %ds\nendTime %v\n",
	// 	latencyThresholdSeconds,
	// 	loopbackPeriodSeconds,
	// 	endTimepb.AsTime())
	ctx2 := context.WithValue(ctx1, endTimeKey{}, endTimepb)

	d := time.Duration(latencyThresholdSeconds) * time.Second
	fmt.Printf("start sleeping for the: %v min\n", d.Minutes())
	bar := progressbar.Default(100)
	for i := 0; i < 100; i++ {
		bar.Add(1)
		time.Sleep(d / 100)
	}
	return context.WithValue(ctx2, sID{}, serviceID), nil
}

func waitingForSecondsToIngestLogbasedMetrics(ctx context.Context, monitoringIngestionTimeSec float64) (context.Context, error) {
	d := time.Duration(monitoringIngestionTimeSec) * time.Second
	fmt.Printf("start sleeping for the: %v min\n", d.Minutes())
	bar := progressbar.Default(100)
	for i := 0; i < 100; i++ {
		bar.Add(1)
		time.Sleep(d / 100)
	}
	return ctx, nil
}

// This step never fails, just arn, as the issue may be related to delayed or missing realtime feed on deleted objects
func thereIsNoAssetInTheScopeWithAnUpdateTimeOlderThanTheJobStarttime(ctx context.Context) (context.Context, error) {
	projectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}
	jobID, ok := ctx.Value(jID{}).(string)
	if !ok {
		return ctx, errors.New("there is no jobID")
	}
	jobStartTime, ok := ctx.Value(jobStartTimeKey{}).(time.Time)
	if !ok {
		return ctx, errors.New("there is no jobStartTime")
	}
	startTimestamp := jobStartTime.Format(time.RFC3339)
	endTimestamp := jobStartTime.Add(10 * time.Minute).Format((time.RFC3339))

	cliLog, err := logadmin.NewClient(ctx, fmt.Sprintf("projects/%s", projectID))
	if err != nil {
		return ctx, err
	}
	filter := fmt.Sprintf(`resource.type="cloud_run_revision" resource.labels.service_name="launch" timestamp >= "%s" timestamp <= "%s" jsonPayload.message="finish %s"`,
		startTimestamp,
		endTimestamp,
		jobID)
	// fmt.Println(filter)
	//  By default, log entries are listed from oldest to newest https://pkg.go.dev/google.golang.org/cloud@v0.34.0/logging/logadmin#NewestFirst
	entryIterator := cliLog.Entries(ctx, logadmin.Filter(filter))
	// There should be only one matching log entry per test run
	logEntry, err := entryIterator.Next()
	if err != nil {
		return ctx, fmt.Errorf("%v on filter %s", err, filter)
	}
	// fmt.Println(reflect.TypeOf(logEntry.Payload))
	payload, ok := logEntry.Payload.(*structpb.Struct)
	if !ok {
		return ctx, errors.New("there is no payload")
	}
	var entry glo.Entry
	b, err := payload.MarshalJSON()
	if err != nil {
		return ctx, err
	}
	err = json.Unmarshal(b, &entry)
	if err != nil {
		return ctx, err
	}
	rootTriggerID := entry.StepStack[0].StepID
	rootTriggerTimestamp := entry.StepStack[0].StepTimestamp
	fmt.Printf("Triggering ID and timestamp %s %v\n", rootTriggerID, rootTriggerTimestamp)

	c, err := bigquery.NewClient(ctx, projectID)
	if err != nil {
		return ctx, err
	}

	sqlCode := `
SELECT
    persistTimestamp,
    COUNT(name) as count
FROM
    ` + "`<project_id>.ram.last_assets`" + `
WHERE
    deleted = false
    AND assetType != "k8s.io/Pod"
    AND scheduledRootTriggeringID != ""
    AND persistTimestamp < "<timestamp>"
GROUP BY
    persistTimestamp
ORDER BY
    persistTimestamp
`

	sqlCode = strings.ReplaceAll(sqlCode, "<project_id>", projectID)
	sqlCode = strings.ReplaceAll(sqlCode, "<timestamp>", rootTriggerTimestamp.Format("2006-01-02 15:04:05.000"))
	// fmt.Printf("%s\n", sqlCode)
	q := c.Query(sqlCode)
	rowIterator, err := q.Read(ctx)
	if err != nil {
		return ctx, err
	}
	for {
		var values []bigquery.Value
		err := rowIterator.Next(&values)
		if err == iterator.Done {
			break
		}
		if err != nil {
			return ctx, err
		}
		fmt.Printf("WARNING Older assets than the triggered export are found, which may be due to delayed realtime feed on deleted assets. The query is :\n%s ", sqlCode)
		break
	}
	ctx1 := context.WithValue(ctx, rTriggerID{}, rootTriggerID)
	return context.WithValue(ctx1, rootTriggerTs{}, rootTriggerTimestamp), nil
}

func filteringOnTheTriggeringJobThereAreAssetsTheCountOfComplianceStatusEqualTheCountOfAssetsForEachRuledeployedOnAGivenAssetTypeExcludingIAMRules(ctx context.Context) (context.Context, error) {
	projectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}
	rootTriggerID, ok := ctx.Value(rTriggerID{}).(string)
	if !ok {
		return ctx, errors.New("there is no rootTriggerID")
	}
	c, err := bigquery.NewClient(ctx, projectID)
	if err != nil {
		return ctx, err
	}
	sqlCode := `
SELECT
	assetType,
	COUNT(name) as count
FROM
` + "`<project_id>.ram.last_assets`" + `
WHERE
	assetType != "k8s.io/Pod"
	AND scheduledRootTriggeringID = "<rootTriggerID>"
GROUP BY
	assetType
ORDER BY
  assetType ASC
`
	sqlCode = strings.ReplaceAll(sqlCode, "<project_id>", projectID)
	sqlCode = strings.ReplaceAll(sqlCode, "<rootTriggerID>", rootTriggerID)
	// fmt.Printf("%s\n", sqlCode)
	q := c.Query(sqlCode)
	rowIterator, err := q.Read(ctx)
	if err != nil {
		return ctx, err
	}
	var countAssettype int
	type assetTypeCount struct {
		AssetType string
		Count     int
	}
	for {
		var r assetTypeCount
		err := rowIterator.Next(&r)
		if err == iterator.Done {
			break
		}
		if err != nil {
			return ctx, err
		}
		countAssettype++
		fmt.Printf("%s %d\n", r.AssetType, r.Count)

		countAssetsByRuleForAnAssetType := `
SELECT
	ruleName,
	COUNT(assetName) AS count
FROM
	` + "`<project_id>.ram.last_compliancestatus`" + `
WHERE
	scheduledRootTriggeringID = "<rootTriggerID>"
	AND assetType = "<assetType>"
	AND NOT CONTAINS_SUBSTR(ruleName, "IAM")
GROUP BY
	ruleName
ORDER BY
	ruleName ASC
`
		countAssetsByRuleForAnAssetType = strings.ReplaceAll(countAssetsByRuleForAnAssetType, "<project_id>", projectID)
		countAssetsByRuleForAnAssetType = strings.ReplaceAll(countAssetsByRuleForAnAssetType, "<rootTriggerID>", rootTriggerID)
		countAssetsByRuleForAnAssetType = strings.ReplaceAll(countAssetsByRuleForAnAssetType, "<assetType>", r.AssetType)
		// fmt.Printf("%s\n", countAssetsByRuleForAnAssetType)
		q = c.Query(countAssetsByRuleForAnAssetType)
		rowIterator2, err := q.Read(ctx)
		if err != nil {
			return ctx, err
		}
		type ruleNameCount struct {
			RuleName string
			Count    int
		}
		for {
			var r2 ruleNameCount
			err := rowIterator2.Next(&r2)
			if err == iterator.Done {
				break
			}
			if err != nil {
				return ctx, err
			}
			if r.Count != r2.Count {
				return ctx, fmt.Errorf("Want %d compliance status for assetType %s rule %s and got %d", r.Count, r.AssetType, r2.RuleName, r2.Count)
			}
			fmt.Printf("%d %s %s\n", r2.Count, r.AssetType, r2.RuleName)
		}
	}
	if countAssettype == 0 {
		return ctx, fmt.Errorf("Want many assets and got none using query:\n%s", sqlCode)
	}
	return ctx, nil
}

func filteringOnTheTriggeringJobThereIsNoViolationWithARelatedComplianceStatusAsCompliant(ctx context.Context) (context.Context, error) {
	projectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}
	rootTriggerID, ok := ctx.Value(rTriggerID{}).(string)
	if !ok {
		return ctx, errors.New("there is no rootTriggerID")
	}
	c, err := bigquery.NewClient(ctx, projectID)
	if err != nil {
		return ctx, err
	}
	sqlCode := `
SELECT
    COUNT(v.nonCompliance.evaluationTimeStamp) as count
FROM
    ` + "`<project_id>.ram.active_violations`" + ` AS v
    INNER JOIN ` + "`<project_id>.ram.last_compliancestatus`" + ` AS s ON v.functionConfig.functionName = s.ruleName
    AND functionConfig.deploymentTime = s.ruleDeploymentTimeStamp
    AND v.feedMessage.asset.name = s.assetName
    AND v.feedMessage.window.startTime = s.assetInventoryTimeStamp
WHERE
    v.feedMessage.asset.scheduledRootTriggeringID = "<rootTriggerID>"
    AND s.compliant
`
	sqlCode = strings.ReplaceAll(sqlCode, "<project_id>", projectID)
	sqlCode = strings.ReplaceAll(sqlCode, "<rootTriggerID>", rootTriggerID)
	// fmt.Printf("%s\n", sqlCode)
	q := c.Query(sqlCode)
	rowIterator, err := q.Read(ctx)
	if err != nil {
		return ctx, err
	}
	type result struct {
		Count int
	}
	var r result
	// Expecting exactly one row
	err = rowIterator.Next(&r)
	if err != nil {
		return ctx, err
	}
	if r.Count != 0 {
		return ctx, fmt.Errorf("Want no violation with a related compliance status to compliant and found %d", r.Count)
	}
	return ctx, nil
}

func filteringOnTheTriggeringJobThereIsNoViolationWithANullUpdateTime(ctx context.Context) (context.Context, error) {
	projectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}
	rootTriggerID, ok := ctx.Value(rTriggerID{}).(string)
	if !ok {
		return ctx, errors.New("there is no rootTriggerID")
	}
	c, err := bigquery.NewClient(ctx, projectID)
	if err != nil {
		return ctx, err
	}
	sqlCode := `
SELECT
    nonCompliance.evaluationTimeStamp
FROM
    ` + "`<project_id>.ram.violations`" + `
WHERE
    feedMessage.asset.scheduledRootTriggeringID = "<rootTriggerID>"
    AND feedMessage.asset.updateTime IS NULL
`
	sqlCode = strings.ReplaceAll(sqlCode, "<project_id>", projectID)
	sqlCode = strings.ReplaceAll(sqlCode, "<rootTriggerID>", rootTriggerID)
	// fmt.Printf("%s\n", sqlCode)
	q := c.Query(sqlCode)
	rowIterator, err := q.Read(ctx)
	if err != nil {
		return ctx, err
	}

	for {
		var values []bigquery.Value
		err := rowIterator.Next(&values)
		if err == iterator.Done {
			break
		}
		if err != nil {
			return ctx, err
		}
		return ctx, fmt.Errorf("Violation with NULL updateTime are found using the query:\n%s ", sqlCode)
	}
	return ctx, nil
}

func filteringOnTheTriggeringJobThereIsNoNotCompliantStatusWithoutAViolationToExplainWhy(ctx context.Context) (context.Context, error) {
	projectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}
	rootTriggerID, ok := ctx.Value(rTriggerID{}).(string)
	if !ok {
		return ctx, errors.New("there is no rootTriggerID")
	}
	c, err := bigquery.NewClient(ctx, projectID)
	if err != nil {
		return ctx, err
	}
	sqlCode := `
SELECT
    COUNT(s.evaluationTimeStamp) as count
FROM
    ` + "`<project_id>.ram.last_compliancestatus`" + ` AS s
    LEFT JOIN ` + "`<project_id>.ram.active_violations`" + ` AS v ON v.functionConfig.functionName = s.ruleName
    AND functionConfig.deploymentTime = s.ruleDeploymentTimeStamp
    AND v.feedMessage.asset.name = s.assetName
    AND v.feedMessage.window.startTime = s.assetInventoryTimeStamp
WHERE
    v.feedMessage.asset.scheduledRootTriggeringID = "<rootTriggerID>"
    AND s.notCompliant
    AND v.functionConfig IS NULL
`
	sqlCode = strings.ReplaceAll(sqlCode, "<project_id>", projectID)
	sqlCode = strings.ReplaceAll(sqlCode, "<rootTriggerID>", rootTriggerID)
	// fmt.Printf("%s\n", sqlCode)
	q := c.Query(sqlCode)
	rowIterator, err := q.Read(ctx)
	if err != nil {
		return ctx, err
	}
	type result struct {
		Count int
	}
	var r result
	// Expecting exactly one row
	err = rowIterator.Next(&r)
	if err != nil {
		return ctx, err
	}
	if r.Count != 0 {
		return ctx, fmt.Errorf("Want no not compliant status without a violation to explain why and found %d", r.Count)
	}
	return ctx, nil
}

func filteringOnTheTriggeringJobThereIsOnlyOneLastComplianceStatusPerRulePerAsset(ctx context.Context) (context.Context, error) {
	projectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}
	rootTriggerID, ok := ctx.Value(rTriggerID{}).(string)
	if !ok {
		return ctx, errors.New("there is no rootTriggerID")
	}
	c, err := bigquery.NewClient(ctx, projectID)
	if err != nil {
		return ctx, err
	}
	sqlCode := `
SELECT
    COUNT(1) AS count,
    assetInventoryTimeStamp,
    assetName,
    ruleName,
    ruleDeploymentTimeStamp
FROM
    ` + "`<project_id>.ram.last_compliancestatus`" + `
WHERE
    scheduledRootTriggeringID = "<rootTriggerID>"
GROUP BY
    assetName,
    assetInventoryTimeStamp,
    ruleName,
    ruleDeploymentTimeStamp
ORDER BY
    count DESC,
    assetInventoryTimeStamp DESC
`
	sqlCode = strings.ReplaceAll(sqlCode, "<project_id>", projectID)
	sqlCode = strings.ReplaceAll(sqlCode, "<rootTriggerID>", rootTriggerID)
	// fmt.Printf("%s\n", sqlCode)
	q := c.Query(sqlCode)
	rowIterator, err := q.Read(ctx)
	if err != nil {
		return ctx, err
	}
	type result struct {
		Count                   int
		AssetInventoryTimeStamp time.Time
		AssetName               string
		RuleName                string
		ruleDeploymentTimeStamp time.Time
	}
	var r result
	// Get the count from from the first row is enough as ordered by count descending
	err = rowIterator.Next(&r)
	if err != nil {
		return ctx, err
	}
	if r.Count > 1 {
		return ctx, fmt.Errorf("Found more than one last compliancestatus for at least one rule one asset using query:\n%s", sqlCode)
	}
	return ctx, nil
}

func theBurnrateIsSmallerThanForSloOnALoopbackPeriodAlignedToTheTest(ctx context.Context, burnRateMax float64, sloName string) (context.Context, error) {
	projectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}
	serviceID, ok := ctx.Value(sID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}
	endTime, ok := ctx.Value(endTimeKey{}).(timestamppb.Timestamp)
	if !ok {
		return ctx, errors.New("there is no endTime")
	}
	loopbackPeriodSeconds, ok := ctx.Value(loopbackPeriodSecondsKey{}).(int64)
	if !ok {
		return ctx, errors.New("there is no loopbackPeriodSeconds")
	}
	c, err := monitoring.NewMetricClient(ctx)
	if err != nil {
		return ctx, err
	}
	filter := fmt.Sprintf("select_slo_burn_rate(\"projects/%s/services/%s/serviceLevelObjectives/%s\", \"%ds\")",
		projectID,
		serviceID,
		sloName,
		loopbackPeriodSeconds)

	// PageSize to 1 as we request 1 data point on the GAUGE
	req := &monitoringpb.ListTimeSeriesRequest{
		Name:   fmt.Sprintf("projects/%s", projectID),
		Filter: filter,
		Interval: &monitoringpb.TimeInterval{
			EndTime: &endTime,
		},
		PageSize: 1,
	}
	it := c.ListTimeSeries(ctx, req)
	// only one iteration as pageSize is one by design
	timeSeries, err := it.Next()
	if err != nil {
		return ctx, fmt.Errorf("No data found for slo %s err %v", sloName, err)
	}
	//Only one data point as we request a GAUGE without a StartTime
	v := timeSeries.Points[0].Value
	burnRateAchieved := v.GetDoubleValue()
	if burnRateAchieved > burnRateMax {
		return ctx, fmt.Errorf("%s burnRateAchieved %f GREATER then burnRateMax %f", sloName, burnRateAchieved, burnRateMax)
	}
	// fmt.Printf("%v %s burnrate\n", burnRateAchieved, sloName)
	return ctx, nil
}

func theAssetOrContentTypeIs(ctx context.Context, contentOrAssetType string) (context.Context, error) {
	return context.WithValue(ctx, contentOrAssetT{}, contentOrAssetType), nil
}

func theRuleWithConstraintIsDeployed(ctx context.Context, ruleName string, constraintName string) (context.Context, error) {
	ramProjectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}
	contentOrAssetType, ok := ctx.Value(contentOrAssetT{}).(string)
	if !ok {
		return ctx, errors.New("there is no contentOrAssetType")
	}
	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		return ctx, err
	}
	ruleRepoBucketName := fmt.Sprintf("%s-rulesrepo", ramProjectID)
	// fmt.Printf("ruleRepoBucketName %s\n", ruleRepoBucketName)
	rulesRepoBucket := storageClient.Bucket(ruleRepoBucketName)
	// fmt.Printf("contentOrAssetType %s\n", contentOrAssetType)
	reader, err := rulesRepoBucket.Object(contentOrAssetType).NewReader(ctx)
	if err != nil {
		return ctx, err
	}
	defer reader.Close()
	b, err := io.ReadAll(reader)
	if err != nil {
		return ctx, err
	}
	var ruleConfigs fetchrules.RuleConfigs
	err = jsoniter.Unmarshal(b, &ruleConfigs)
	if err != nil {
		return ctx, err
	}
	found := false
	for rName, rConfig := range ruleConfigs {
		for cName := range rConfig.ConstraintConfigs {
			if rName == ruleName && cName == constraintName {
				found = true
			}
		}
	}
	if !found {
		return ctx, fmt.Errorf("Did not found rule %s with constraint %s for contentOrAssetType %s", contentOrAssetType, ruleName, constraintName)
	}
	ctx1 := context.WithValue(ctx, constrName{}, constraintName)
	return context.WithValue(ctx1, ruName{}, ruleName), nil
}

func theResourceProjectIs(ctx context.Context, projectID string) (context.Context, error) {
	switch projectID {
	case "your-resource-project-id-with-autofix":
		projectID = os.Getenv("AUTOFIX_ON_PROJECT_ID")
	case "your-resource-project-id-without-autofix":
		projectID = os.Getenv("AUTOFIX_OFF_PROJECT_ID")
	case "your-resource-project-id-autofix-changing":
		projectID = os.Getenv("AUTOFIX_CHANGING_PROJECT_ID")
	case "your-resource-project-id-for-rule-test":
		projectID = os.Getenv("PROJECT_ID_FOR_RULE_TEST")
	}
	return context.WithValue(ctx, resourceProjectID{}, projectID), nil
}

func theTagKeyWithTagValueIsToTheResourceProject(ctx context.Context, tagKeyShortName string, tagValueShortName string, wantedStatus string) (context.Context, error) {
	rProjectID, ok := ctx.Value(resourceProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no resource projectID")
	}
	cloudResourceManagerService, err := cloudresourcemanager.NewService(ctx)
	if err != nil {
		return ctx, err
	}
	projectService := cloudresourcemanager.NewProjectsService(cloudResourceManagerService)
	p, err := projectService.Get(fmt.Sprintf("projects/%s", rProjectID)).Context(ctx).Do()
	if err != nil {
		return ctx, err
	}
	// fmt.Printf("%s\n", p.Name)
	effectiveTagService := cloudresourcemanager.NewEffectiveTagsService(cloudResourceManagerService)
	fullResourceName := fmt.Sprintf("//cloudresourcemanager.googleapis.com/%s", p.Name)
	// fmt.Printf("fullResourceName %s", fullResourceName)
	browseEffectiveTags := func(resp *cloudresourcemanager.ListEffectiveTagsResponse) error {
		effectiveTags := resp.EffectiveTags
		for _, effectiveTag := range effectiveTags {
			// fmt.Printf("tagKeyShortName %s\ntagValueShortName %s\neffectiveTag.NamespacedTagValue %s\n", tagKeyShortName, tagValueShortName, effectiveTag.NamespacedTagValue)
			if strings.Contains(effectiveTag.NamespacedTagValue, fmt.Sprintf("/%s/%s", tagKeyShortName, tagValueShortName)) {
				return fmt.Errorf("found")
			}
		}
		return nil
	}
	err = effectiveTagService.List().Parent(fullResourceName).Pages(ctx, browseEffectiveTags)
	if err != nil {
		if err.Error() == "found" {
			if wantedStatus == "unbinded" {
				return ctx, fmt.Errorf("Want keytag %s with keyValue %s to be unbinded on %s and found it binded", tagKeyShortName, tagValueShortName, rProjectID)
			}
		} else {
			return ctx, err
		}
	} else {
		if wantedStatus == "binded" {
			return ctx, fmt.Errorf("Want keytag %s with keyValue %s to be binded on %s and found it unbinded", tagKeyShortName, tagValueShortName, rProjectID)
		}
	}
	return context.WithValue(ctx, tagValShortName{}, tagValueShortName), nil
}

func aBigquerydatasetIsCreatedInLocation(ctx context.Context, location string) (context.Context, error) {
	rProjectID, ok := ctx.Value(resourceProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no resource projectID")
	}
	bqClient, err := bigquery.NewClient(ctx, rProjectID)
	if err != nil {
		return ctx, err
	}
	now := time.Now()
	// RFC3339Nano looks like "2006-01-02T15:04:05Z07:00"
	datasetName := strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(fmt.Sprintf("auto_test_%s_%s", location, now.Format(time.RFC3339Nano)), "-", "_"), ":", "_"), ".", "_"), "+", "plus")
	var datasetMetadata bigquery.DatasetMetadata
	datasetMetadata.Location = location
	datasetMetadata.Description = "Real-time Asset Monitor auto test end to end"
	dataset := bqClient.DatasetInProject(rProjectID, datasetName)
	if err := dataset.Create(ctx, &datasetMetadata); err != nil {
		return ctx, err
	}
	ctx1 := context.WithValue(ctx, jobStartTimeKey{}, now)
	return context.WithValue(ctx1, dsName{}, datasetName), nil
}

func theComplianceHasBeenAnalyzedInLessThanMinutes(ctx context.Context, minutes float64) (context.Context, error) {
	projectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}
	rProjectID, ok := ctx.Value(resourceProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no resource projectID")
	}
	jobStartTime, ok := ctx.Value(jobStartTimeKey{}).(time.Time)
	if !ok {
		return ctx, errors.New("there is no jobStartTime")
	}
	ruleName, ok := ctx.Value(ruName{}).(string)
	if !ok {
		return ctx, errors.New("there is no ruleName")
	}
	datasetName, ok := ctx.Value(dsName{}).(string)
	if !ok {
		return ctx, errors.New("there is no datasetName")
	}
	startTimestamp := jobStartTime.Format(time.RFC3339)
	endTimestamp := jobStartTime.Add(10 * time.Minute).Format((time.RFC3339))
	cliLog, err := logadmin.NewClient(ctx, fmt.Sprintf("projects/%s", projectID))
	if err != nil {
		return ctx, err
	}
	filter := fmt.Sprintf(`resource.type="cloud_run_revision" resource.labels.service_name="monitor" timestamp >= "%s" timestamp <= "%s" jsonPayload.assetInventoryOrigin="real-time" jsonPayload.message:"finish" jsonPayload.ruleName="%s" jsonPayload.step_stack.step_id:"//bigquery.googleapis.com/projects/%s/datasets/%s"`,
		startTimestamp,
		endTimestamp,
		ruleName,
		rProjectID,
		datasetName)
	// fmt.Println(filter)
	//  By default, log entries are listed from oldest to newest https://pkg.go.dev/google.golang.org/cloud@v0.34.0/logging/logadmin#NewestFirst
	entryIterator := cliLog.Entries(ctx, logadmin.Filter(filter))
	// There should be only one matching log entry per test run
	logEntry, err := entryIterator.Next()
	if err != nil {
		return ctx, fmt.Errorf("The compliance has not been analyzed, error %v on filter %s", err, filter)
	}
	// fmt.Println(reflect.TypeOf(logEntry.Payload))
	payload, ok := logEntry.Payload.(*structpb.Struct)
	if !ok {
		return ctx, errors.New("there is no payload")
	}
	var entry glo.Entry
	b, err := payload.MarshalJSON()
	if err != nil {
		return ctx, err
	}
	err = json.Unmarshal(b, &entry)
	if err != nil {
		return ctx, err
	}
	if entry.LatencyE2ESeconds > 60*minutes {
		return ctx, fmt.Errorf("The compliance has been analyzed in more than %v minutes: %v %s", minutes, entry.LatencyE2ESeconds/60, entry.StepStack[0].StepID)
	}
	fmt.Printf("LatencyE2ESeconds %v root StepID %s\n", entry.LatencyE2ESeconds, entry.StepStack[0].StepID)
	return ctx, nil
}

func theAssetBeFound(ctx context.Context, status string) (context.Context, error) {
	rProjectID, ok := ctx.Value(resourceProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no resource projectID")
	}
	datasetName, ok := ctx.Value(dsName{}).(string)
	if !ok {
		return ctx, errors.New("there is no resource projectID")
	}
	bqClient, err := bigquery.NewClient(ctx, rProjectID)
	if err != nil {
		return ctx, err
	}
	dataset := bqClient.DatasetInProject(rProjectID, datasetName)
	_, err = dataset.Metadata(ctx)
	if err != nil {
		if status == "can" {
			return ctx, fmt.Errorf("want dataset %s and got error when fetching metadata %v", datasetName, err)
		}
		return ctx, nil
	}
	if status == "cannot" {
		return ctx, fmt.Errorf("want dataset %s to be deleted and still found it", datasetName)
	}
	return ctx, nil
}

func aLogEntryTraceTheDeleteActionAs(ctx context.Context, status string) (context.Context, error) {
	projectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}
	rProjectID, ok := ctx.Value(resourceProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no resource projectID")
	}
	jobStartTime, ok := ctx.Value(jobStartTimeKey{}).(time.Time)
	if !ok {
		return ctx, errors.New("there is no jobStartTime")
	}
	ruleName, ok := ctx.Value(ruName{}).(string)
	if !ok {
		return ctx, errors.New("there is no ruleName")
	}
	datasetName, ok := ctx.Value(dsName{}).(string)
	if !ok {
		return ctx, errors.New("there is no datasetName")
	}
	tagValueShortName, ok := ctx.Value(tagValShortName{}).(string)
	if !ok {
		return ctx, errors.New("there is no tagValueShortName")
	}
	startTimestamp := jobStartTime.Format(time.RFC3339)
	endTimestamp := jobStartTime.Add(10 * time.Minute).Format((time.RFC3339))
	cliLog, err := logadmin.NewClient(ctx, fmt.Sprintf("projects/%s", projectID))
	if err != nil {
		return ctx, err
	}
	filter := fmt.Sprintf(`resource.type="cloud_run_revision" resource.labels.service_name="autofixbqdsdelete" timestamp >= "%s" timestamp <= "%s" jsonPayload.message:"finish %s" jsonPayload.ruleName="%s" jsonPayload.step_stack.step_id:"//bigquery.googleapis.com/projects/%s/datasets/%s"`,
		startTimestamp,
		endTimestamp,
		tagValueShortName,
		ruleName,
		rProjectID,
		datasetName)
	// fmt.Println(filter)
	//  By default, log entries are listed from oldest to newest https://pkg.go.dev/google.golang.org/cloud@v0.34.0/logging/logadmin#NewestFirst
	entryIterator := cliLog.Entries(ctx, logadmin.Filter(filter))
	// There should be only one matching log entry per test run
	logEntry, err := entryIterator.Next()
	if err != nil {
		return ctx, fmt.Errorf("The delete action log entry was not found, error %v on filter %s", err, filter)
	}
	// fmt.Println(reflect.TypeOf(logEntry.Payload))
	payload, ok := logEntry.Payload.(*structpb.Struct)
	if !ok {
		return ctx, errors.New("there is no payload")
	}
	var entry glo.Entry
	b, err := payload.MarshalJSON()
	if err != nil {
		return ctx, err
	}
	err = json.Unmarshal(b, &entry)
	if err != nil {
		return ctx, err
	}
	if !strings.Contains(entry.Message, status) {
		return ctx, fmt.Errorf("Want %s finish log entry message contains %s and found %s", tagValueShortName, status, entry.Message)
	}
	return ctx, nil
}

func dataIsAddedToTheDataset(ctx context.Context) (context.Context, error) {
	rProjectID, ok := ctx.Value(resourceProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no resource projectID")
	}
	datasetName, ok := ctx.Value(dsName{}).(string)
	if !ok {
		return ctx, errors.New("there is no resource projectID")
	}
	bqClient, err := bigquery.NewClient(ctx, rProjectID)
	if err != nil {
		return ctx, err
	}
	dataset := bqClient.DatasetInProject(rProjectID, datasetName)
	tableName := strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(fmt.Sprintf("auto_test_%s", time.Now().Format(time.RFC3339Nano)), "-", "_"), ":", "_"), ".", "_"), "+", "plus")
	var tableMetadata bigquery.TableMetadata
	tableMetadata.Description = "Real-time Asset Monitor auto test end to end"
	err = dataset.Table(tableName).Create(ctx, &tableMetadata)
	if err != nil {
		return ctx, err
	}
	return ctx, nil
}

func theDatasetIsUpdated(ctx context.Context) (context.Context, error) {
	rProjectID, ok := ctx.Value(resourceProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no resource projectID")
	}
	datasetName, ok := ctx.Value(dsName{}).(string)
	if !ok {
		return ctx, errors.New("there is no resource projectID")
	}
	bqClient, err := bigquery.NewClient(ctx, rProjectID)
	if err != nil {
		return ctx, err
	}
	dataset := bqClient.DatasetInProject(rProjectID, datasetName)
	var metadata2Update bigquery.DatasetMetadataToUpdate
	metadata2Update.Description = fmt.Sprintf("Real-time Asset Monitor autofix test end to end updated %v", time.Now())
	_, err = dataset.Update(ctx, metadata2Update, "")
	if err != nil {
		return ctx, err
	}
	return ctx, nil
}

func theLastAssetComplianceStatusToThisRuleIs(ctx context.Context, status string) (context.Context, error) {
	projectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}
	rProjectID, ok := ctx.Value(resourceProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no resource projectID")
	}
	ruleName, ok := ctx.Value(ruName{}).(string)
	if !ok {
		return ctx, errors.New("there is no ruleName")
	}
	datasetName, ok := ctx.Value(dsName{}).(string)
	if !ok {
		return ctx, errors.New("there is no datasetName")
	}
	c, err := bigquery.NewClient(ctx, projectID)
	if err != nil {
		return ctx, err
	}
	sqlCode := `
SELECT
	compliant,
	deleted,
	assetName,
	ruleName,
	assetInventoryTimeStamp,
	evaluationTimeStamp
FROM
  ` + "`<project_id>.ram.last_compliancestatus`" + `
WHERE
	ruleName = "<ruleName>"
	AND assetName = "//bigquery.googleapis.com/projects/<rProjectID>/datasets/<datasetName>"
`
	sqlCode = strings.ReplaceAll(sqlCode, "<project_id>", projectID)
	sqlCode = strings.ReplaceAll(sqlCode, "<rProjectID>", rProjectID)
	sqlCode = strings.ReplaceAll(sqlCode, "<ruleName>", ruleName)
	sqlCode = strings.ReplaceAll(sqlCode, "<datasetName>", datasetName)
	// fmt.Printf("%s\n", sqlCode)
	q := c.Query(sqlCode)
	rowIterator, err := q.Read(ctx)
	if err != nil {
		return ctx, err
	}
	type complianceStatus struct {
		Compliant               bool
		Deleted                 bool
		AssetName               string
		RuleName                string
		assetInventoryTimeStamp time.Time
		evaluationTimeStamp     time.Time
	}
	var r complianceStatus
	// Expecting exactly one last compliance status for this asset this rule
	err = rowIterator.Next(&r)
	if err != nil {
		return ctx, err
	}
	switch status {
	case "false":
		if r.Compliant {
			return ctx, fmt.Errorf("want last compliance status to be %s and got %v for rule %s on dataset %s", status, r.Compliant, ruleName, datasetName)
		}
	case "true":
		if !r.Compliant {
			return ctx, fmt.Errorf("want last compliance status to be %s and got %v for rule %s on dataset %s", status, r.Compliant, ruleName, datasetName)
		}
	default:
		return ctx, fmt.Errorf("Unsupported status %s", status)
	}
	return ctx, nil
}

func theCountOfActiveViolationsIs(ctx context.Context, wantActiveViolationCount int64) (context.Context, error) {
	projectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}
	rProjectID, ok := ctx.Value(resourceProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no resource projectID")
	}
	ruleName, ok := ctx.Value(ruName{}).(string)
	if !ok {
		return ctx, errors.New("there is no ruleName")
	}
	constraintName, ok := ctx.Value(constrName{}).(string)
	if !ok {
		return ctx, errors.New("there is no constraintName")
	}
	datasetName, ok := ctx.Value(dsName{}).(string)
	if !ok {
		return ctx, errors.New("there is no datasetName")
	}
	c, err := bigquery.NewClient(ctx, projectID)
	if err != nil {
		return ctx, err
	}
	sqlCode := `
SELECT
	nonCompliance.message
FROM
	` + "`<project_id>.ram.active_violations`" + `
WHERE
	functionConfig.functionName = "<ruleName>"
	AND constraintConfig.metadata.name = "<constraintName>"
	AND feedMessage.asset.name = "//bigquery.googleapis.com/projects/<rProjectID>/datasets/<datasetName>"
`
	sqlCode = strings.ReplaceAll(sqlCode, "<project_id>", projectID)
	sqlCode = strings.ReplaceAll(sqlCode, "<rProjectID>", rProjectID)
	sqlCode = strings.ReplaceAll(sqlCode, "<ruleName>", ruleName)
	sqlCode = strings.ReplaceAll(sqlCode, "<constraintName>", constraintName)
	sqlCode = strings.ReplaceAll(sqlCode, "<datasetName>", datasetName)
	// fmt.Printf("%s\n", sqlCode)
	q := c.Query(sqlCode)
	rowIterator, err := q.Read(ctx)
	if err != nil {
		return ctx, err
	}
	var haveActiveViolationCount int64
	for {
		var values []bigquery.Value
		err := rowIterator.Next(&values)
		if err == iterator.Done {
			break
		}
		if err != nil {
			return ctx, err
		}
		haveActiveViolationCount++
	}
	if haveActiveViolationCount != wantActiveViolationCount {
		return ctx, fmt.Errorf("want %d active violations and got %d", wantActiveViolationCount, haveActiveViolationCount)
	}
	return ctx, nil
}

func theLastAssetDeleteFlagIs(ctx context.Context, status string) (context.Context, error) {
	projectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}
	rProjectID, ok := ctx.Value(resourceProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no resource projectID")
	}
	datasetName, ok := ctx.Value(dsName{}).(string)
	if !ok {
		return ctx, errors.New("there is no datasetName")
	}
	c, err := bigquery.NewClient(ctx, projectID)
	if err != nil {
		return ctx, err
	}
	sqlCode := `
SELECT
	deleted,
	name,
	timeStamp,
	persistTimeStamp
FROM
  ` + "`<project_id>.ram.last_assets`" + `
WHERE
	name = "//bigquery.googleapis.com/projects/<rProjectID>/datasets/<datasetName>"
`
	sqlCode = strings.ReplaceAll(sqlCode, "<project_id>", projectID)
	sqlCode = strings.ReplaceAll(sqlCode, "<rProjectID>", rProjectID)
	sqlCode = strings.ReplaceAll(sqlCode, "<datasetName>", datasetName)
	// fmt.Printf("%s\n", sqlCode)
	q := c.Query(sqlCode)
	rowIterator, err := q.Read(ctx)
	if err != nil {
		return ctx, err
	}
	type asset struct {
		Deleted          bool
		Name             string
		TimeStamp        time.Time
		PersistTimeStamp time.Time
	}
	var r asset
	// Expecting exactly one last compliance status for this asset this rule
	err = rowIterator.Next(&r)
	if err != nil {
		return ctx, err
	}
	switch status {
	case "false":
		if r.Deleted {
			return ctx, fmt.Errorf("want last asset to be %s and got %v for dataset %s", status, r.Deleted, datasetName)
		}
	case "true":
		if !r.Deleted {
			return ctx, fmt.Errorf("want last asset to be %s and got %v for dataset %s", status, r.Deleted, datasetName)
		}
	default:
		return ctx, fmt.Errorf("Unsupported status %s", status)
	}
	return ctx, nil
}

func theCountOfLogEntriesWithOriginNotEqualToIs(ctx context.Context, microserviceName string, origin string, count int) (context.Context, error) {
	projectID, ok := ctx.Value(ramProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no projectID")
	}
	startTime := time.Now()
	startTimestamp := startTime.Format(time.RFC3339)
	endTimestamp := startTime.Add(24 * time.Hour).Format((time.RFC3339))
	cliLog, err := logadmin.NewClient(ctx, fmt.Sprintf("projects/%s", projectID))
	if err != nil {
		return ctx, err
	}
	filter := fmt.Sprintf(`resource.type="cloud_run_revision" resource.labels.service_name="%s" timestamp >= "%s" timestamp <= "%s" jsonPayload.assetInventoryOrigin!="%s" jsonPayload.message:"finish"`,
		microserviceName,
		startTimestamp,
		endTimestamp,
		origin)
	// fmt.Println(filter)
	//  By default, log entries are listed from oldest to newest https://pkg.go.dev/google.golang.org/cloud@v0.34.0/logging/logadmin#NewestFirst
	entryIterator := cliLog.Entries(ctx, logadmin.Filter(filter))
	// There should be only one matching log entry per test run
	countFound := entryIterator.PageInfo().Remaining()
	if countFound != count {
		return ctx, fmt.Errorf("Want %d log entries and found %d with filter %s", count, countFound, filter)
	}
	return ctx, nil
}

func theTagKeyWithTagValueToTheResourceProject(ctx context.Context, action string, tagKeyShortName string, tagValueShortName string) (context.Context, error) {
	rProjectID, ok := ctx.Value(resourceProjectID{}).(string)
	if !ok {
		return ctx, errors.New("there is no resource projectID")
	}
	cloudResourceManagerService, err := cloudresourcemanager.NewService(ctx)
	if err != nil {
		return ctx, err
	}
	projectService := cloudresourcemanager.NewProjectsService(cloudResourceManagerService)
	folderService := cloudresourcemanager.NewFoldersService(cloudResourceManagerService)
	tagKeyService := cloudresourcemanager.NewTagKeysService(cloudResourceManagerService)
	tagValueService := cloudresourcemanager.NewTagValuesService(cloudResourceManagerService)
	tagBindingService := cloudresourcemanager.NewTagBindingsService(cloudResourceManagerService)
	operationService := cloudresourcemanager.NewOperationsService(cloudResourceManagerService)
	// Get the parent organization
	p, err := projectService.Get(fmt.Sprintf("projects/%s", rProjectID)).Context(ctx).Do()
	if err != nil {
		return ctx, err
	}
	var parentOrg string
	parent := p.Parent
	for {
		if strings.Contains(parent, "organizations/") {
			parentOrg = parent
			break
		} else {
			if strings.Contains(parent, "folders/") {
				var f *cloudresourcemanager.Folder
				f, err = folderService.Get(fmt.Sprintf(parent)).Context(ctx).Do()
				if err != nil {
					break
				}
				parent = f.Parent
			} else {
				err = fmt.Errorf("Unexpected parent %s", parent)
				break
			}
		}
	}
	if err != nil {
		return ctx, err
	}
	// fmt.Printf("parentOrg %s\n", parentOrg)

	// Get autofix tag key name
	var autofixTagKeyName string
	browseKey := func(resp *cloudresourcemanager.ListTagKeysResponse) error {
		tagKeys := resp.TagKeys
		for _, tagKey := range tagKeys {
			if tagKey.ShortName == tagKeyShortName {
				autofixTagKeyName = tagKey.Name
				break
			}
		}
		return nil
	}
	err = tagKeyService.List().Parent(parentOrg).Pages(ctx, browseKey)
	if err != nil {
		return ctx, err
	}
	if autofixTagKeyName == "" {
		return ctx, fmt.Errorf("Did not find key %s in org %s", tagKeyShortName, parentOrg)
	}
	// fmt.Printf("autofixTagKeyName %s\n", autofixTagKeyName)

	// Get autofix tag value name
	var autofixTagValueName string
	browseValue := func(resp *cloudresourcemanager.ListTagValuesResponse) error {
		tagValues := resp.TagValues
		for _, tagValue := range tagValues {
			if tagValue.ShortName == tagValueShortName {
				autofixTagValueName = tagValue.Name
				break
			}
		}
		return nil
	}
	err = tagValueService.List().Parent(autofixTagKeyName).Pages(ctx, browseValue)
	if err != nil {
		return ctx, err
	}
	if autofixTagValueName == "" {
		return ctx, fmt.Errorf("Did not find value %s in key %s", tagValueShortName, tagKeyShortName)
	}
	// fmt.Printf("autofixTagValueName %s\n", autofixTagValueName)

	// Check if already binded
	var tagBindingName string
	browseBinding := func(resp *cloudresourcemanager.ListTagBindingsResponse) error {
		tagBindings := resp.TagBindings
		for _, tagBinding := range tagBindings {
			if tagBinding.TagValue == autofixTagValueName {
				tagBindingName = tagBinding.Name
				break
			}
		}
		return nil
	}
	err = tagBindingService.List().Parent(fmt.Sprintf("//cloudresourcemanager.googleapis.com/%s", p.Name)).Pages(ctx, browseBinding)
	if err != nil {
		return ctx, err
	}
	// fmt.Printf("tagBindingName %s\n", tagBindingName)

	// do the action
	var tagBinding cloudresourcemanager.TagBinding
	tagBinding.Parent = fmt.Sprintf("//cloudresourcemanager.googleapis.com/%s", p.Name)
	tagBinding.TagValue = autofixTagValueName
	var opsName string
	if tagBindingName != "" {
		// already binded
		if action == "unbind" {
			ops, err := tagBindingService.Delete(tagBindingName).Context(ctx).Do()
			if err != nil {
				return ctx, err
			}
			opsName = ops.Name
			fmt.Printf("unbind opsName %s\n", opsName)
		}
	} else {
		// not already binded
		if action == "bind" {
			ops, err := tagBindingService.Create(&tagBinding).Context(ctx).Do()
			if err != nil {
				return ctx, err
			}
			opsName = ops.Name
			fmt.Printf("bind opsName %s\n", opsName)
		}
	}
	if opsName != "" {
		var i time.Duration
		opsDone := false
		for i <= 30 {
			d := i * time.Second
			time.Sleep(d)
			ops, err := operationService.Get(opsName).Context(ctx).Do()
			if err != nil {
				if !strings.Contains(err.Error(), "404") {
					return ctx, fmt.Errorf("operationService.Get %v", err)
				}
			}
			if ops.Done {
				opsDone = true
				break
			} else {
				fmt.Printf("Ops not yet done, waiting iteration %v %v sec", i, d.Seconds())
			}
			i++
		}
		if !opsDone {
			return ctx, fmt.Errorf("Operation to bind or unbind did not complete on time %s", opsName)
		}
		fmt.Printf("operation done %s\n", opsName)
	} else {
		fmt.Printf("already set as expected no action required\n")
	}
	return ctx, nil
}

func InitializeScenario(ctx *godog.ScenarioContext) {

	ctx.Step(`^(.+) the tag key (.+) with tag value (.+) to the resource project$`, theTagKeyWithTagValueToTheResourceProject)
	ctx.Step(`^a bigquerydataset is created in location (.+)$`, aBigquerydatasetIsCreatedInLocation)
	ctx.Step(`^a log entry trace the delete action as (.+)$`, aLogEntryTraceTheDeleteActionAs)
	ctx.Step(`^data is added to the dataset$`, dataIsAddedToTheDataset)
	ctx.Step(`^filtering on the triggering job there are assets the count of compliance status equal the count of assets for each rule deployed on a given asset type excluding IAM rules$`, filteringOnTheTriggeringJobThereAreAssetsTheCountOfComplianceStatusEqualTheCountOfAssetsForEachRuledeployedOnAGivenAssetTypeExcludingIAMRules)
	ctx.Step(`^filtering on the triggering job there is no not compliant status without a violation to explain why$`, filteringOnTheTriggeringJobThereIsNoNotCompliantStatusWithoutAViolationToExplainWhy)
	ctx.Step(`^filtering on the triggering job there is no violation with a null update time$`, filteringOnTheTriggeringJobThereIsNoViolationWithANullUpdateTime)
	ctx.Step(`^filtering on the triggering job there is no violation with a related compliance status as compliant$`, filteringOnTheTriggeringJobThereIsNoViolationWithARelatedComplianceStatusAsCompliant)
	ctx.Step(`^filtering on the triggering job there is only one last compliance status per rule per asset$`, filteringOnTheTriggeringJobThereIsOnlyOneLastComplianceStatusPerRulePerAsset)
	ctx.Step(`^the (.+) file has been copied to the (.+) gcs bucket$`, theFileHasBeenCopiedToTheGcsBucket)
	ctx.Step(`^the asset (.+) be found$`, theAssetBeFound)
	ctx.Step(`^the asset or content type is (.+)$`, theAssetOrContentTypeIs)
	ctx.Step(`^the burnrate is smaller than (\d+) for (.+) slo on a loopback period aligned to the test$`, theBurnrateIsSmallerThanForSloOnALoopbackPeriodAlignedToTheTest)
	ctx.Step(`^the compliance has been analyzed in less than (\d+) minutes$`, theComplianceHasBeenAnalyzedInLessThanMinutes)
	ctx.Step(`^the count of active violations is (\d+)$`, theCountOfActiveViolationsIs)
	ctx.Step(`^the count of (.+) log entries with origin not equal to (.+) is (\d+)$`, theCountOfLogEntriesWithOriginNotEqualToIs)
	ctx.Step(`^the dataset is updated$`, theDatasetIsUpdated)
	ctx.Step(`^the job (.+) located in (.+) is manually triggered$`, theJobLocatedInIsManuallyTriggered)
	ctx.Step(`^the last asset compliance status to this rule is (.+)$`, theLastAssetComplianceStatusToThisRuleIs)
	ctx.Step(`^the last asset delete flag is (.+)$`, theLastAssetDeleteFlagIs)
	ctx.Step(`^the ram project is (.+)$`, theRAMProjectIs)
	ctx.Step(`^the resource project is (.+)$`, theResourceProjectIs)
	ctx.Step(`^the rule (.+) with constraint (.+) is deployed$`, theRuleWithConstraintIsDeployed)
	ctx.Step(`^the tag key (.+) with tag value (.+) is (.+) to the resource project$`, theTagKeyWithTagValueIsToTheResourceProject)
	ctx.Step(`^there is no asset in the scope with an update time older than the job starttime$`, thereIsNoAssetInTheScopeWithAnUpdateTimeOlderThanTheJobStarttime)
	ctx.Step(`^waiting for (\d+) seconds to ingest logbased metrics$`, waitingForSecondsToIngestLogbasedMetrics)
	ctx.Step(`^waiting for the latency threshold of (.+) service (.+) slo$`, waitingForTheLatencyThresholdOfServiceSlo)

	ctx.After(func(ctx context.Context, sc *godog.Scenario, err error) (context.Context, error) {
		rProjectID, ok := ctx.Value(resourceProjectID{}).(string)
		if ok {
			datasetName, ok2 := ctx.Value(dsName{}).(string)
			if ok2 {
				bqClient, err := bigquery.NewClient(ctx, rProjectID)
				if err != nil {
					return ctx, err
				}
				dataset := bqClient.DatasetInProject(rProjectID, datasetName)
				err = dataset.DeleteWithContents(ctx)
				if err != nil {
					fmt.Printf("Cleanup cannot deleted the dataset %s %s %v\n", rProjectID, datasetName, err)
				} else {
					fmt.Printf("Cleanup deleted the dataset %s %s\n", rProjectID, datasetName)
				}
			}
		}
		return ctx, nil
	})

}

func InitializeTestSuite(sc *godog.TestSuiteContext) {
	ramProjectID := os.Getenv("FETCHRULES_PROJECT_ID")
	if ramProjectID == "" {
		log.Fatalln("Missing environment variable FETCHRULES_PROJECT_ID")
	}
	rulesRepoBucketName := ramProjectID + "-rulesrepo"
	logOnlySeverityLevels := os.Getenv("FETCHRULES_LOG_ONLY_SEVERITY_LEVELS")
	if ramProjectID == "" {
		log.Fatalln("Missing environment variable FETCHRULES_LOG_ONLY_SEVERITY_LEVELS")
	}
	location := os.Getenv("FETCHRULES_CRUN_REGION")
	if ramProjectID == "" {
		log.Fatalln("Missing environment variable FETCHRULES_CRUN_REGION")
	}
	if err := fetchrules.DeployRules(context.Background(),
		"./rules",
		"",
		rulesRepoBucketName,
		ramProjectID,
		logOnlySeverityLevels,
		true,
		location); err != nil {
		log.Fatalf("fetchrules.DeployRules %v\n", err)
	}
}

func TestFeatures(t *testing.T) {
	suite := godog.TestSuite{
		TestSuiteInitializer: InitializeTestSuite,
		ScenarioInitializer:  InitializeScenario,
		Options: &godog.Options{
			Format:   "pretty",
			Paths:    []string{"features"},
			TestingT: t, // Testing instance that will run subtests.
		},
	}

	if suite.Run() != 0 {
		t.Fatal("non-zero status returned, failed to run feature tests")
	}
}
