# RAM test

Steps:

- Release compliance rules
- Release scheduled actions
- Use End to End test to check RAM is up and running
- Visualize non compliance findings in looker Studio

## Release compliance rules

- [Install GO](https://golang.org/doc/install) using the same version as specified in ./test/go.mod
- Build local RAM cli
  - `cd test`
  - `go mod tidy`
  - `go build ram.go`
- Set environment variables. This can be a `set_env_vars.sh` file to be run with `source ./set_env_vars.sh` like this one:

```shell
# $1 is the environment, test or prod. test by default. do not use dev that is reserved for the project hosting docker container images, code unit and integration tests
if [[ -z "$1" ]]; then
    environment="test"
else
    environment=$1
fi
if [ $environment == "test" ] || [ $environment == "prod" ]; then
    if [ $environment == "prod" ]; then
        export FETCHRULES_PROJECT_ID="your-gcp-project-id-production"
        export FETCHRULES_ENVIRONMENT="prod"
    else
        export FETCHRULES_PROJECT_ID="your-gcp-project-id-test"
        export FETCHRULES_ENVIRONMENT="test"
    fi
    echo FETCHRULES_PROJECT_ID $FETCHRULES_PROJECT_ID
    echo  FETCHRULES_ENVIRONMENT $FETCHRULES_ENVIRONMENT
    export GOOGLE_APPLICATION_CREDENTIALS="/your/path/to/terraform_account_key.json"
else
    echo "Error - environment must be 'test' or 'prod'"
fi
export FETCHRULES_LOG_ONLY_SEVERITY_LEVELS="WARNING NOTICE CRITICAL"
echo FETCHRULES_LOG_ONLY_SEVERITY_LEVELS $FETCHRULES_LOG_ONLY_SEVERITY_LEVELS```
```

- The folder`./test/rules` contains a sample set of compliance rules. To be REVIEWED, adapted to your context and completed by new rules as needed.
  - You could start also be keeping only one rule in this folder, and deploy additional rules incrementally, the ease the remediation effort.
  - The way files are organized in the rule folder is up to you, including using or not subfolders for clarity, and naming each file as you wish but respecting `.rego` and `.yaml` extensions.
  - Content of the file MUST respect the following:
    - Each rule MUST have one `.rego` file containing a package statement with the rule name as `templates.gcp.yourRuleName`, and of course the rego code.
    - Their MUST be at least one constraint .`yaml` file per `.rego` rule containing
      - a `kind` token that match the rule name
      - one of:
        - a `metadata/annotations/assetType` token matching the assetType this rule apply
        - a `metadata/annotations/contentType` set to `IAM_POLICY` when the rule is about GCP IAM policy analysis
- Publish the local rule(s) to the cloud:
  - `./ram`
  - The ram cli output looks like:

```shell
ProjectID <yourProjectID>
Environment qa
LogOnlySeveritylevels WARNING NOTICE CRITICAL
RulesRepoBucketName <yourProjectID>-rulesrepo

parseRuleFiles completed without error 29 rules 35 constraints 35 readmes
rulesRepo built
rulerepo.json file written locally
rulerepo.csv file written locally

Got rulesRepoBucket handle without error
rulesRepo updated to gcs bucket <yourProjectID>-rulesrepo
```

- Check results:
  - In the related GCS bucket named `your-project-id-rulesrepo` unless you set a different name.
  - Locally reviewing `rulerepo.csv` and `rulerepo.json` contents

## Release scheduled actions

- Adapt the `test/actions.json` file to configure scheduled actions, using the following guidelines:
  - The level one token must match the text payload used in cloud schedulers e.g. `at_minute_0_past_every_3rd_hour`
  - There is no requirement to use only one action file. actions may be split in multiple file if it eases role segregation and or config code review.
- Release the action file using `gsutil cp ./actions.json gs://your-project-id-actionsrepo`

## Use End to End acceptance tests to check RAM is up and running

- Review, adapt the `test/features/assessment_scheduled.feature` Gherkin feature specification with:
  - Your project
  - Your job name
  - Any other settings you changed
- Execute the test by running: `godog run`
- Once this test is complete it is worth triggering it a second time to ensure this cache is fully populated, avoiding non human friendly names in the reports like org number or folder numbers.

## Visualize non compliance findings in Datastudio

- Follow instructions on the [Looker studio template report](https://datastudio.google.com/u/0/reporting/893a1cd0-1c9d-4b64-9c1a-2415f0d3e75d/page/tPRSB) README page to instantiate the report in you environment.
- Overwise you can also query BigQuery from the console, select your project / `ram` data set
  - Select `active_violation` view / query
  - Select `last_compliancestatus` view / query
  - Select `last_assets` view / query

Done. Your RAM core instance is operational
