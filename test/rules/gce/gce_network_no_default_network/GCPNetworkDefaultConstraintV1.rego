# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
package templates.gcp.GCPNetworkDefaultConstraintV1

import data.validator.gcp.lib as lib
import future.keywords.in

deny[{
	"msg": message,
	"details": metadata,
}] {
	constraint := input.constraint
	lib.get_constraint_params(constraint, params)
	mode := lib.get_default(params, "mode", "denylist")
	network := lib.get_default(params, "name", "default")
	target_match_count(mode, desired_count)

	asset := input.asset
	asset.asset_type == "compute.googleapis.com/Network"

	# Check if resource is in exempt list
	exempt_list := lib.get_default(params, "exemptions", []) #  params.exemptions
	not asset.name in exempt_list

	network_name := asset.resource.data.name
	trace(sprintf("network_name: %v", [network_name]))

	matches_found = [n | n = network[_]; network_name == n]
	trace(sprintf("Violation found  if %v != %v", [desired_count, count(matches_found)]))
	count(matches_found) != desired_count

	message := sprintf("Network %v is present.", [network_name])
	metadata := {"resource": asset.name}
}

target_match_count(mode) = 0 {
	mode == "denylist"
}

target_match_count(mode) = 1 {
	mode == "allowlist"
}
