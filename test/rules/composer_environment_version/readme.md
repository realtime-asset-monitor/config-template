# composer_environment_version

## Background

A Cloud Composer environment image version is [supported for 12 months](https://cloud.google.com/composer/docs/concepts/versioning/composer-versioning-overview#version-deprecation-and-support) after the release date.

## Fix

[Upgrade environments](https://cloud.google.com/composer/docs/how-to/managing/upgrading)

## References

- [Cloud Composer images list with release dates](https://cloud.google.com/composer/docs/concepts/versioning/composer-versions#images)
- [Cloud Composer version deprecation and support](https://cloud.google.com/composer/docs/concepts/versioning/composer-versioning-overview#version-deprecation-and-support)
