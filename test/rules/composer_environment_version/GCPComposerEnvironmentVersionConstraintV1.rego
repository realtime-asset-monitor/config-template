# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
package templates.gcp.GCPComposerEnvironmentVersionConstraintV1

import data.validator.gcp.lib as lib
import future.keywords.in

############################################
# Find BigQuery Dataset Location Violations
############################################
deny[{
	"msg": message,
	"details": metadata,
}] {
	constraint := input.constraint
	lib.get_constraint_params(constraint, params)

	# Verify that resource is BigQuery dataset
	asset := input.asset
	asset.asset_type == "composer.googleapis.com/Environment"
	trace(sprintf("asset.asset_type", [asset.asset_type]))

	# Check if resource is in exempt list
	exempt_list := lib.get_default(params, "exemptions", []) #  params.exemptions
	not asset.name in exempt_list

	# Check tversion
	asset_version := asset.resource.data.config.softwareConfig.imageVersion
	releaseDateList := params.releaseDateList
	releaseDateList[asset_version]
	releaseDate := releaseDateList[asset_version]
	trace(sprintf("asset_version %v releaseDate %v", [asset_version, releaseDate]))

	fullSupportPeriod := params.fullSupportPeriod
	trace(sprintf("fullSupportPeriod %v", [fullSupportPeriod]))

	delay_ns := time.parse_duration_ns(fullSupportPeriod)
	trace(sprintf("delay_ns %v", [delay_ns]))

	releaseTime := concat("", [releaseDate, "T01:00:06Z"])
	trace(sprintf("releaseTime %v", [releaseTime]))

	releaseTime_ns := time.parse_rfc3339_ns(releaseTime)
	trace(sprintf("releaseTime_ns %v", [releaseTime_ns]))

	now := time.now_ns()
	now > releaseTime_ns + delay_ns # not compliant when now if above the end of support time

	message := sprintf("asset %v uses an out of support image which version is %v that was released on %v and supported for %v after the release date", [asset.name, asset_version, releaseDate, fullSupportPeriod])
	metadata := {
		"version": asset_version,
		"fullSupportPeriod": fullSupportPeriod,
	}
}
