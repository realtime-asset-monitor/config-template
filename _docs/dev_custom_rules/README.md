# Local setup to develop and test custom rules

## Install OPA locally

- Check which version of OPA RAM uses from [`go.mod`](https://gitlab.com/realtime-asset-monitor/monitor/-/blob/main/go.mod)
- Install the same version of OPA locally using [Running OPA documentation](https://www.openpolicyagent.org/docs/latest/#running-opa), and add it to the PATH
- Check OPA version using `opa version`

## Test the example rule

- The example rules is in the subfolder `example01` in the folder containing this README. It is the Bigquery location rule.
- From the folder containing this README, run: `opa run example01/`. Result looks like:

```shell
opa run example01/
OPA 0.50.1 (commit c2af620726b29635d1827ab04d344d4b39d773fa, built at 2023-03-16T11:48:14Z)

Run 'help' to see a list of commands and check for updates.

> 
```

- From OPA run use 3 following commands:

```shell
> package validator.gcp.lib
> notes
> audit
```

- The expected output contains 1) the note traces 2) the violation array, example

```shell
query:1                                                              Enter data.validator.gcp.lib.audit = _
example01/modules/audit.rego:21                                      | Enter data.validator.gcp.lib.audit
example01/modules/audit.rego:24                                      | | Note "asset name: //bigquery.googleapis.com/projects/brunore-ram-noncompliant/datasets/brunore_ds_01"
example01/modules/audit.rego:31                                      | | Note "constraint kind: GCPBigQueryDatasetLocationConstraintV1"
example01/modules/audit.rego:51                                      | | Note "asset.ancestry_path: organization/012345678901/folder/123456789012/project/23456789012"
example01/modules/audit.rego:52                                      | | Note "targets: [\"organization/\"]"
example01/modules/audit.rego:53                                      | | Note "is in scope:%!(EXTRA string=true)"
example01/modules/audit.rego:67                                      | | Note "exclusions: null"
example01/modules/audit.rego:68                                      | | Note "Excluded if count exclusion_match > 0: 0"
example01/modules/GCPBigQueryDatasetLocationConstraintV1.rego:23     | | Enter data.templates.gcp.GCPBigQueryDatasetLocationConstraintV1.deny
example01/modules/GCPBigQueryDatasetLocationConstraintV1.rego:42     | | | Note "mode: allowlist, asset_location:US, target_locations:[\"EU\", \"europe-north1\", \"europe-west1\", \"europe-west3\", \"europe-west4\"]"
example01/modules/audit.rego:21                                      | Redo data.validator.gcp.lib.audit
example01/modules/audit.rego:24                                      | | Note "asset name: //bigquery.googleapis.com/projects/brunore-ram-noncompliant/datasets/brunore_ds_02"
example01/modules/audit.rego:31                                      | | Note "constraint kind: GCPBigQueryDatasetLocationConstraintV1"
example01/modules/audit.rego:51                                      | | Note "asset.ancestry_path: organization/012345678901/folder/123456789012/project/23456789012"
example01/modules/audit.rego:52                                      | | Note "targets: [\"organization/\"]"
example01/modules/audit.rego:53                                      | | Note "is in scope:%!(EXTRA string=true)"
example01/modules/audit.rego:67                                      | | Note "exclusions: null"
example01/modules/audit.rego:68                                      | | Note "Excluded if count exclusion_match > 0: 0"
example01/modules/GCPBigQueryDatasetLocationConstraintV1.rego:23     | | Enter data.templates.gcp.GCPBigQueryDatasetLocationConstraintV1.deny
example01/modules/GCPBigQueryDatasetLocationConstraintV1.rego:42     | | | Note "mode: allowlist, asset_location:EU, target_locations:[\"EU\", \"europe-north1\", \"europe-west1\", \"europe-west3\", \"europe-west4\"]"
[
  {
    "asset": "//bigquery.googleapis.com/projects/brunore-ram-noncompliant/datasets/brunore_ds_01",
    "constraint": "bq_dataset_location_europe",
    "constraint_config": {
      "apiVersion": "constraints.gatekeeper.sh/v1alpha1",
      "kind": "GCPBigQueryDatasetLocationConstraintV1",
      "metadata": {
        "annotations": {
          "assetType": "bigquery.googleapis.com/Dataset",
          "category": "Personal Data Compliance",
          "description": "BQ Dataset must be located in Europe."
        },
        "name": "bq_dataset_location_europe"
      },
      "spec": {
        "match": {
          "exclude": null,
          "target": [
            "organization/"
          ]
        },
        "parameters": {
          "exemptions": [],
          "locations": [
            "EU",
            "europe-north1",
            "europe-west1",
            "europe-west3",
            "europe-west4"
          ],
          "mode": "allowlist"
        },
        "severity": "critical"
      }
    },
    "violation": {
      "details": {
        "location": "US"
      },
      "msg": "//bigquery.googleapis.com/projects/brunore-ram-noncompliant/datasets/brunore_ds_01 is in a disallowed location."
    }
  }
]
> 
```

## Update `upload2gcs.yaml` setting file to get asset JSON example

The availability of Cloud Asset Inventory asset JSON files samples facilitates the development of new custom compliance.

This is the purpose of the microservice `upload2gcs`: It uploads, to a GCS bucket named by default `<projectID>-assetjson`, the content of the `asset` object of the Cloud Asset Inventory feed that have been converted/enriched by `convertfeed` and published to the topic named by default `assetFeed`, as described in the [RAM graph](../ram.svg).

By default this bucket has a lifecycle rule to delete object older than 1 day, to limit costs.

The `upload2gcs.yaml` settings file located in the GCS bucket `<projectID>-attributesrepo` configure which assetTypes and contentTypes are uploaded to `<projectID>-assetjson`. The default setting create by the terraform module during the initial setup is set to only upload organization assets of RESOURCE contentType:

```yaml
pubSubAttributeName: upload2gcs
pubSubAttributeValue: true
# The content type suffix RESOURCE can be omitted as it is added by default when no content type is specified
# The content type suffix IAM_POLICY must be specified when need
assetTypeAndContentList:
  - cloudresourcemanager.googleapis.com/Organization
```

Example: The following configuration change will set uploading to GCS Cloud Run assets for both content RESOURCE and IAM_POLICY:

```yaml
pubSubAttributeName: upload2gcs
pubSubAttributeValue: true
# The content type suffix RESOURCE can be omitted as it is added by default when no content type is specified
# The content type suffix IAM_POLICY must be specified when need
assetTypeAndContentList:
  - cloudresourcemanager.googleapis.com/Organization
  - run.googleapis.com/Service
  - run.googleapis.com/ServiceIAM_POLICY
  ```

- Copy the changed `upload2gcs.yaml` to your `<projectID>-assetjson` GCS bucket using `gsutil` or the console upload feature
- Redeploy convertfeed to ensure the cache is refreshed with the new configuration
- Trigger an export
- Check the asset JSON files have been uploaded to the `<projectID>-assetjson` GCS bucket

`upload2gcs` create objects in the bucket with the following suffixes:

- `_iam.json` when Cloud Asset Inventory content type is IAM policy
- `.json` for anything else
