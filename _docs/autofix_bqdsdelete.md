# autofix bqdsdelete

`autofix` is RAM feature for automatic remediation.  
`bqdsdelete` is the use case to automatically delete Bigquery dataset within 5 minutes from creation when the location is not compliant.  
`autofix bqdsdelete` is specified using the Gherkin language in [this feature file](../test/features/autofix_bqdsdelete.feature)  
The Ghekin feature specifications are used to implement the acceptance tests (aka end to end tests) using [cucumber framework](https://github.com/cucumber/godog) implementation in go named `godog` with this [go code](../test/ram_e2e_test.go)

## Primum non nocere

Caution is the top 1 focus when implementing automated remediation, meaning:

1. Is [optional](#optional-installation)
2. Apply only to the real-time feed in a timeframe of [5 minutes](https://gitlab.com/realtime-asset-monitor/autofix/-/blob/main/types.go#L31)
3. Is triggered only by filtered violations specific to one compliance rule, in this used case: `GCPBigQueryDatasetLocationConstraintV1`
4. Deletes only empty datasets calling a BigQuery API througth the GO client that can [only delete when empty](https://pkg.go.dev/cloud.google.com/go/bigquery#Dataset.Delete)
5. Run only where it has been [authorized](#authorizing-autofix-bqdsdelete)
6. [Logs transparently](https://gitlab.com/realtime-asset-monitor/autofix/-/blob/main/func_bqdsdelete.go#L27) all actions
7. Goes through a comprehensive set of tests before releasing: unit, integration **AND acceptance**.

## Optional installation

`autofix bddsdelete` is not installed by default. Define the GCP organizations where to use it in the terrafrom variable `autofix_org_ids`, example:

```terraform
variable "autofix_org_ids" {
  type        = list(string)
  description = "List of organization numbers where to create the autofix tag key and values"
}
```

 Set `deploy_autofix_bqdsdelete` to true to install it. Example:

```terraform
module "realtime-asset-monitor" {
  source                  = "BrunoReboul/realtime-asset-monitor/google"
  version                 = "<THE VERSION>"
  project_id                = local.project_id
  export_org_ids            = var.export_org_ids
  export_folder_ids         = var.export_folder_ids
  feed_iam_policy_folders   = var.feed_iam_policy_folders
  feed_iam_policy_orgs      = var.feed_iam_policy_orgs
  feed_resource_folders     = var.feed_resource_folders
  feed_resource_orgs        = var.feed_resource_orgs
  log_only_severity_levels  = var.log_only_severity_levels
  pubsub_allowed_regions    = var.pubsub_allowed_regions
  gcs_location              = var.gcs_location
  crun_region               = var.crun_region
  dataset_location          = var.dataset_location
  scheduler_region          = var.scheduler_region
  views_interval_days       = var.views_interval_days
  schedulers                = var.schedulers
  notification_channels     = var.notification_channels
  ram_e2e_latency           = var.ram_e2e_latency
  cai_latency               = var.cai_latency
  api_latency               = var.api_latency
  deploy_autofix_bqdsdelete = true
}
```

Once installed there is a new Cloud Run service named `autofixbddsdelete` that use the `autofix` docker container image and for which the `AUTOFIX_ACTION_KIND` environment variable is set to `bqdsdelete`.  

**warning** do not forget to update the tool you are using to upgrade RAM version so that it includes `autofix`: it is not included in the default updgrade templatesas `autofix` is optioal:

- gcloud scripts [deploy_no_traffic.sh](../upgrade/gcloud/deploy_no_traffic.sh) and [update_traffix,sh](../upgrade/gcloud/update_traffic.sh)
- or Cloud Build templates [deploy_no_traffic.yaml](../upgrade/cloudbuild/deploy_no_traffic.yaml) and [update_traffic](../upgrade/cloudbuild/update_traffic.yaml)
- or you own CICD tool

## Authorizing `autofix bqdsdelete`

`autofix` uses a shared respnosability model: a mix of pre configured resources and resources you need to define for the autoremidiation mecanism to be effective.

### The preconfigured resources

- `autofixbqdsdelete` cloud run service runs using its own service account which email is exposed in the terraform output

### The resources you need to set up

- An `autofix` organization tag key in each targeted GCP organization, example:

```terraform
variable "autofix_org_ids" {
  type        = list(string)
  description = "List of organization numbers where to create the autofix tag key and values"
}
resource "google_tags_tag_key" "autofix_key" {
  for_each    = toset(var.autofix_org_ids)
  parent      = "organizations/${each.key}"
  short_name  = "autofix"
  description = "Real-time Asset Monitor automatic remediation"
}
output "autofix_tag_key_ids" {
  value = tomap({ for o in sort(var.autofix_org_ids) : o => google_tags_tag_key.autofix_key[o].id })
}
```

- A `bqdsdelete` organization tag value for `autofix` organization tag key which Id is exposed in the terraform output

```terraform
resource "google_tags_tag_value" "autofix_bqdsdelete_value" {
  for_each    = toset(var.autofix_org_ids)
  parent      = "tagKeys/${google_tags_tag_key.autofix_key[each.key].name}"
  short_name  = "bqdsdelete"
  description = "Real-time Asset Monitor delete Bigquery Dataset"
}
output "ram_autofix_bqdsdelete_tag_value_id" {
  value = { for o in sort(var.autofix_org_ids) : o => google_tags_tag_value.autofix_bqdsdelete_value[o].id }
}
```

- An organization custom role with only `bigquery.datasets.delete` permision. If you do not want to set org custom roles, an alternative is to use the predefined role [`roles/bigquery.admim`](https://cloud.google.com/bigquery/docs/access-control#bigquery.admin) but it will break the least priviledge security principle. Example:

```terraform
resource "google_organization_iam_custom_role" "autofix_bqdsdelete" {
  role_id     = "autofix_bqdsdelete"
  org_id      = "<your org ID>"
  title       = "autofix bqdsdelete"
  description = "Delete dataset"
  permissions = ["bigquery.datasets.delete"]
}
```

- An IAM conditionnal policy on the portion of the resource hierarchy where to use automated remediation. Could be the full org or a subset using a folder. Example:

```terraform
resource "google_folder_iam_member" "bqdsdelete_bq" {
  folder = "folders/<your folder ID>"
  role   = google_organization_iam_custom_role.autofix_bqdsdelete.name
  member = "serviceAccount:${module.realtime-asset-monitor.autofixbqdsdelete[0].service_account_email}"

  condition {
    title       = "autofix bqdsdelete"
    description = "autofix bqdsdelete service account can delete bigquery datasets when the tag value autofix:bqdsdelete is binded or inherited"
    expression  = "resource.matchTagId('tagKeys/<your tag key Id>', 'tagValues/<your tag value Id>')"
  }
}
```

- A tag value binding on the subfolder(s) or project(s) where to acticate `autofix bqdsdelete`. Example:

```terraform
variable "autofix_bqdsdelete_binding_parent_list" {
  type        = map(string)
  description = "The key is the parent string the value is the organization id"
}
resource "google_tags_tag_binding" "autofix_bqdsdelete" {
  for_each  = var.autofix_bqdsdelete_binding_parent_list
  parent    = each.key
  tag_value = "tagValues/<your tag value Id>"
}
```

## Logging

The following query shows the automated remediation activity

```log
resource.type="cloud_run_revision"
resource.labels.service_name="autofixbqdsdelete"
jsonPayload.message:"finish bqdsdelete"
```

The finish messages can be one of:

- finish bqdsdelete done
- finish bqdsdelete cancelled unauthorized
- finish bqdsdelete cancelled as not empty

The payload log entry `description` field contains the triggering violation that contains extensive details on 1. the rule config and code 2. the asset inventory 3. the violation details.

## Analytics

The following SQL query and JSON results shows how autoremediation shows from an analytic perspective:

```sql
SELECT
  evaluationTimeStamp,
  assetInventoryTimeStamp,
  compliant,
  deleted
FROM
  `<your_ram_project_Id>.ram.complianceStatus`
WHERE
  assetName LIKE "%<the_big_query_dataset_name>%"
  AND ruleName = "GCPBigQueryDatasetLocationConstraintV1"
ORDER BY
  evaluationTimeStamp ASC
```

**Example:** With auto remedation new non compliant BQ dataset are deleted on the fly

```json
[{
  "evaluationTimeStamp": "2022-10-03 05:37:51.517899 UTC",
  "assetInventoryTimeStamp": "2022-10-03 05:37:47.789341 UTC",
  "compliant": "false",
  "deleted": "false"
}, {
  "evaluationTimeStamp": "2022-10-03 05:37:55.679210 UTC",
  "assetInventoryTimeStamp": "2022-10-03 05:37:53.242449 UTC",
  "compliant": "true",
  "deleted": "true"
}]
```
