# Known errors

## Real-time flow works but exports flow fails for a given asset type

**Issue**:  
For a given assetType, realtime scanning itself works. But batch export do not work. The export GCS bucket contains the exports delivered by Cloud Asset Inventory, and some child exports.

**Internals**:  
A CAI export is a [Newline Delimited JSON](http://ndjson.org/) file. Each line is the JSON definition of a given asset. `splitexport` microservice uses a GO scanner to read the lines configured with a buffer. The buffer size is defined in the environment variable [SPLITEXPORT_SCANNER_BUFFER_SIZE_KILO_BYTES](https://gitlab.com/realtime-asset-monitor/splitexport/-/blob/main/types.go#L37), the default value is 256. If one asset JSON definition is larger than the buffer, `splitexport` fails to process the export.

**Resolution**:  
Increase the buffer size to a value larger than the largest line in the export to be processed. Monitor `splitexport` memory usage and increase the Cloud Run service memory allocation if needed.

## Missing violations when launching manually a full export just after updating the rule repo

**Issue**:  

- A sustained realtime flow keep `fetchrules` active, meaning not scaling to zero, for example during business hours.
- The rule repo has been updated
- A full export has been manually launched somewhat after
- There are missing violations related to the updated rules.

**Internals**  
`fetchrules` uses an in memory cache of the rules repo. It is populated when [the set of rules for a given asset of content type is not found or is too old](https://gitlab.com/realtime-asset-monitor/fetchrules/-/blob/main/func_make_asset_rules.go#L60). Too old means the age of the set of rules for a given asset of content type is greater than the value defined in the environment variable [FETCHRULES_CACHE_MAX_AGE_MINUTES](https://gitlab.com/realtime-asset-monitor/fetchrules/-/blob/main/types.go#L34). The default value is 60.

So it may happen that some assets from the same export are evaluated using the oldversion of the rule because a `fetchrule` instance was already up to process the realtime flow, while some others are evaluated with the new rule version because a) processed by a new instances provisioned by the autoscalling to address the full export peak b) the rule set was refreshed as older than the configured age.

By design the `active_violation` BigQuery view only show result related to the last version of a rule, meaning it hides the result of the preview version.

**Resolution**:  

- Wait at least the time configured in `FETCHRULES_CACHE_MAX_AGE_MINUTES` between a rule update and a manual export launch (1 hour by default)
- Reduce the `FETCHRULES_CACHE_MAX_AGE_MINUTES`, for example in `test` environment when developping / testing new rules, at the cost of less optimizing `fetchrules` and keep it as is in `prod` environement. It can be set to 0.

## Configuring realtime analysis of Kubernetes pod may lead to significant extra cost

More info in the [gke pod rules example folder](gke_pod_rules/README.md)

## Configuring only export analysis of Kubernetes pod may lead to some false positive

More info in the [gke pod rules example folder](gke_pod_rules/README.md)
