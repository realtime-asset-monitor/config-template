# RAM Setup

Clone this repo to setup Real-time Asset Monitor in your environment.

To explore RAM, follow these links:

1. [Provision the infrastructure and deploy microservices using terraform](install/README.md)
2. [Customize compliance rules and visualize results](test/README.md)
3. [Upgrade the microservices to a new version using the CD pipeline of your choice](upgrade/README.md)

## Serverless components graph

![ram_graph](_docs/ram.svg)

## Next steps

### Additional information

- [FAQ](_docs/faq.md)
- [Known errors](_docs/known_errors.md)
- [Service Level Objectives SLOs](_docs/slos.md)

## RAM release notes

- All microservices container images are tagged with the same value for a given RAM release. These tags are identifiable by the `ram-` prefix. After the prefix the tag follows semantic versioning replacing dots `.` with dash `-` as dots are not supported in Google Artifact Registry tag values.
- In complement each microservices container image is also tagged with its own version. These tags are identifiable by the prefix `git-`. After the prefix the tag follows the git commit short SHA signature used to build this image ensuring traceability between code and container images.
- RAM terraform module git repo and RAM microservices container images share the same two first digits to materialize the dependency between RAM app code and RAM infrastructure code:
  - Example:
    - RAM microservices container images version ram-v0-1-z requires to use terraform module v0.1.t.
    - Where z and t could be any value.
  - This simplify roll back procedure to clearly determine which infra version support which app version
  - Use cases:
    1. The infrastructure changes, for example to use a Pub/Sub pus subscription trigger where the ack_deadline and the filter are configurable in replacement of the an EventArc trigger. This use case requires to synchronize with the App code as the event receiver use a different format.
    2. The App code changes, for example to add a new microservice like `upload2gcs` to facilitate the development of custom compliance rules. This use case requires to synchronize with the Infrastructure code as new resources like a trigger or a GCS bucket must be provisioned.
    3. The infrastructure changes, adding new monitoring dashboards, without requiring any changes on the app code.
    4. The app code changes, refactoring the code, without requiring anu changes on the infrastructure code.

### WARNING - changes required for v-xxx

- New environment variables for microservice `consolebff`
  - Improved security: `consolebff` now validates user token against IAP (see changes below) using the IAP audience to do so
  - `consolebff` terraform module has been updated to add two new environment audience variables, one per backend service, to new RAM deployments
  - It is required to add these variable to whatever deployment tools used when upgrading `consolebff` Cloud Run service to a new revision
  - The required variables are:
    - `CONSOLEBFF_AUDIENCE_ADMIN=/projects/<yourProjectNumber>/global/backendServices/<yourAdminBackend GeneratedId>`
    - `CONSOLEBFF_AUDIENCE_RESULTS=/projects/<yourProjectNumber>/global/backendServices/<yourResultsBackend GeneratedId>`
    - The two `generatedId` can be found in the output of the `loadbalancer` module
    - Or the full audience string could be found in the IAP console three dot menu `Get JWT audience code` for each backend service
  - Implementation examples for [GitLab](https://gitlab.com/realtime-asset-monitor/consolebff/-/blob/main/.gitlab-ci.yml?ref_type=heads#L120)
- Activate API `aiplatform.googleapis.com` Vertex AI API

### terraform module WIP

- [feat(consolebff): grant this service service account the role 'roles/aiplatform.user' to enable the SQL chat bot](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/112)
- [feat[consolebff]Add GCS bucket usersrolerepo to host the usersrole yaml config file](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/110)
- [feat(consolebff): Add backend service id from LB as env vars so that is enable IAP claims validation](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/107)

### ram-WIP

- console
  - [feat: add profile button and popup](https://gitlab.com/realtime-asset-monitor/console/-/issues/3)
  - [chore: upgrade flutter tools and dependencies, refacto go code, refacto ci](https://gitlab.com/realtime-asset-monitor/console/-/issues/2)
    - flutter part:
      - flutter v3.24.3
      - dio 5.2.0
      - create web/index.html to fix warnings:
        - Local variable for "serviceWorkerVersion" is deprecated
        - "FlutterLoader.loadEntrypoint" is deprecated
      - re-generate dart code on last openAPI API definition using last openapi generator version
      - improve doc
    go part:
      - refactor code structure with new server package and new tests
      - fix base URL for new flutter version
      - add structured logger configured for Cloud monitoring format
  - [chore: refacto dart code](https://gitlab.com/realtime-asset-monitor/console/-/issues/1)
  - Upgrade to Flutter v3.24.3
- consolebff
  - [feat: add websocket server side to manage a chat bot](https://gitlab.com/realtime-asset-monitor/consolebff/-/issues/3)
  - [feat: add RBAC feature and related new ops in the API so the front can react on role the user belongs to](https://gitlab.com/realtime-asset-monitor/consolebff/-/issues/2)
    - By default a user has the viewer role which allows to three features:
      - `get_v1userprofile` to get the logged user id and his role
      - `get_v1assetnames` to list the assets names
      - `get_v1timelines` to get an asset x rule compliance timeline
    - The second role is `analyst` which allows the features of `viewer` role plus:
      - `post_v1sendchatmessage` to use the future SQL chat feature to query results
    - There is no need to assign the `viewer` role to most of the users as it is assigned by default. To assign other roles:
      - Future: use Cloud Identify Platform to add the role to the claims. `consolebff` is already able to get the role from the token payload claims.
      - When the role is not found in the claims, `consolebff` try to find a match from a file names `usersrole.yaml` and located in the new GCS bucket named `<your-project-id-usersrolerepo>`. the file cached in memory at startup, and when older then the value configured in  the environment `variable CONSOLEBFF_CACHE_MAX_AGE_MINUTES` that default to 60 when not set. The file format is:

        ```yaml
        # viewer roles is by default
        someone-email: analyst
        someone-else-email: analyst
        ```

  - [feat: Validate IAP token and get claims to improve security and enable user profiles and roles](https://gitlab.com/realtime-asset-monitor/consolebff/-/issues/1)
    - refactor code
    - improve logging
    - migrate to oapi-codegen v2

### terraform module v0.9.2

- [Make the publish2fs local-command to add the TTL to firestore optional](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/105)

### ram-v0-9-1

- [Monitor does not retry even when rego asks for a retry](https://gitlab.com/realtime-asset-monitor/monitor/-/issues/16)
- [Improve expireat tool performance and retry on timeout when dealing with large asset collection](https://gitlab.com/realtime-asset-monitor/setup/-/issues/10)

### terraform module v0.9.1

- [fix(pushSub): config push endpoint to use the topic id](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/103)

### terraform module v0.9.0 - Deprecated

- [feat(publish2fs):add retention policy on asset collection](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/98)
  - There is no terraform resource to [create a TTL policy](https://firebase.google.com/docs/firestore/ttl#gcloud) on a firestore DB.
  - Using the generic [terraform-google-gcloud](https://registry.terraform.io/modules/terraform-google-modules/gcloud/google/latest) module
  - The command execution take several minutes
- [fix(console*): limit invoker role to iap service account](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/95)
- [chore(cloudrun): update to google_cloud_run_v2_service terraform resources](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/101)
  - More info on [Cloud Run service API v2 in terraform](https://cloud.google.com/blog/products/devops-sre/migrating-terraform-resources-stablely-to-cloud-run-api-version-2)
  - The migration strategy is to let terraform destroy the cloud run services and recreate them, which mean loosing the revision history
  - In case you have supercharged some cloud run services check that:
    - `crun_cpu` variable value format changes. Example before "1000m" now "1"
    - `crun_timeout_seconds` variable replaced by `crun_timeout` and the value format changes from e.g. 180 to "180s"
    - `status[0].url` computed attribute is replaced by `uri` used in the related Pubsub subscriptions
  - Known error: Resource already exists but was marked for deletion. Just wait for the Cloud Run services to be deleted, apply again.

### ram-v0-9-0 Deprecated

- publish2fs
  - Leverage firestore retention policy to delete orphan asset documents that may lead to false positive for example when a linked asset rule counts the number of Cloud Run revision for a cloud Run service.
    - Code change:[eat(entrypoint): compute persist expireAt timestamp to be leveraged by firestore TTL retention policy](https://gitlab.com/realtime-asset-monitor/publish2fs/-/issues/2)
    - The default retention duration is 8 days, configurable with env var `PUBLISH2FS_ASSET_RETENTION_DAYS`
    - By default resources that belong to resource manager are exclude, meaning org, folder et projects. configurable with env var `PUBLISH2FS_ASSET_RETENTION_EXEMPTION`. Setting this variable to en empty string deactivate the feature.
    - The tool [expireat](tool/expireat/README.md) may be used to set an expireat timestamp on document already in the firestore `assets` collection
- monitor
  - [feat(inspectResultSet): manage transient errors when HTTP call are made from the rule s REGO code](https://gitlab.com/realtime-asset-monitor/monitor/-/issues/15)
- fetchrules
  - [fix(fetchrules): refactor redeployCloudRunRevision code to handle TRAFFIC_TARGET_ALLOCATION_TYPE_LATEST and TRAFFIC_TARGET_ALLOCATION_TYPE_REVISION](https://gitlab.com/realtime-asset-monitor/fetchrules/-/issues/21)
  - [fix(makeAssetRules): When contentType specified as RESOURCE the rule is not fetched](https://gitlab.com/realtime-asset-monitor/fetchrules/-/issues/20)
  - Your ram client need to be updated:
    - option on: copy it from the [setup repo test folder](https://gitlab.com/realtime-asset-monitor/setup/-/tree/main/test?ref_type=heads)
    - option two:  
      - Update your `go.mod` to reference fetchrules package version v0.0.11 like this [go.mod](https://gitlab.com/realtime-asset-monitor/setup/-/blob/main/test/go.mod?ref_type=heads)
      - `go get -u -t ./...`
      - `go mod tidy`
      - `go build ram.go`
- utilities
  - package `cai` add `ExpireAt` timestamp field to `FeedMessageFS` type
- go v1.22.5, OPA v0.66.0

### terraform module v0.8.0

- [feat(fetchrules): add gcs bucket for common Rego code and preprovision it](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/93)
- [feat(monitor):add roles/datastore.viewer to enable requesting firestore docs from REGO code](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/91)
- [Fix typo in attributes config files pubSubAttributeName](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/89)
  - The terraform module will NOT update existing `upload2gcs.yaml` and `publish2fs.yaml` files in the gcs bucket `attributesrepo`. You should fix the typo manually on existing files by adding the missing `r` to pubSubAttributeName.
  - For fresh install the Terraform module will create the files with the correct value.

### ram-v0-8-0

- go v1.22.0, OPA v0.61.0
- convertfeed
  - [feat(getAssetRules): load common Rego code from gcs instead of static inclusion in go code so that it allows additional custom common Rego code](https://gitlab.com/realtime-asset-monitor/fetchrules/-/issues/19)
  - [feat(deployRules): Make rule deployment timestamp equal when using the same rule on multiple asset / content types](https://gitlab.com/realtime-asset-monitor/fetchrules/-/issues/18)
  - [Fix typo in attributes config files pubSubAttributeName](https://gitlab.com/realtime-asset-monitor/convertfeed/-/issues/13)
    - See above comment onTerraform module to update existing `upload2gcs.yaml` and `publish2fs.yaml` files
  - New feature: [I want to specify both content type and asset type in a rule constraint](https://gitlab.com/realtime-asset-monitor/fetchrules/-/issues/18) so that the rule is evaluated only for this combination saving energy, money & time:

| If a constraint specifies | Then | Example |
| --- | --- | --- |
| Only a content type | the related rule apply to any asset type | generic IAM rules apply to all asset IAM policies |
| Only an asset type | the rule apply to this asset type and the content type RESOURCE that is assumed implicitly (Aka no content type means content type is RESOURCE) | most of existing rules |
| Both an asset type and a content type | the rule applies specifically to this combination, limiting effectively execution scope | A rule to check IAM policy for Cloud Run services only |

### terraform module v0.7.1

- [refactor log based metric names](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/86)
  - In addition to the label change in previous release (v07.0) the log base metrics names have been refactored for more clarity.
  - Due to hidden links, and despite the existing explicit depend_on at module level, theses changes to log base metric crash the terraform apply with an `Error 400: Cannot delete metric that is still used in an alerting policy`
  - To work around this issue, there is now a new boolean setting `deploy_slos` on the `realtime-asset-monitor` module set to `true` by default module. If you upgrade to v.0.7.1, please:
    - Set `deploy_slos` to `false`, which will remove the resources from modules `slos`, `slos_cai`, `transparentslis` and `dashboards`.
    - Run `terraform plan`, `terraform apply`.
    - Remove the setting `deploy_slos`, as its default value is `true` this will recreate the resources from modules `slos`, `slos_cai`, `transparentslis` and `dashboards` on the next `apply`.
    - Run `terraform plan`, `terraform apply`.
  - The release `v0.7.0` has been deleted, please use `v0.7.1` directly.
- [Set deletion_protection=false in order to destroy resource "google_bigquery_table"](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/85)
  - Instead of changing the `deletion_protection` setting from `true` to `false` it is now exposed from the module `realtime-asset-monitor` as variable `bq_tables_deletion_protection` with a default value to `true` that you can change to `false`

### terraform module v0.7.0 - release deprecated

- convertfeed tf module
  - [convertfeed module - add publish2fs.yaml default pubsub attribute config file](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/68)
    - **WARNING** Review the default `publish2fs.yaml` in the gcs bucket `attributesrepo`. To adapt it: download / change / upload. The changed version will be preserved by terraform as the related terraform resource uses lifecycle ignore_changes all.
  - [convertfeed module - add upload2gcs.yaml default pubsub attribute config file](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/77)
- publish2fs tf module
  - [publish2fs module - adapt subscription filter to new convertfeed customizable message attributes](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/75)
    - **WARNING** Review the default `upload2gcs.yaml` in the gcs bucket attributesrepo. To adapt it: download / change / upload. The changed version will be preserved by terraform as the related terraform resource uses lifecycle ignore_changes all.
- upload2gcs module
  - [upload2gcs module - adapt subscription filter to new convertfeed customizable message attributes](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/79)
    - **warning** this filter is now authoritative. As indicated above, review-adapt the file upload2gcs.yaml in the attributesrepo gcs bucket.
  - [Tune upload2gcs max concurrent requests to 12 and memory to 512MB](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/83)
- autofixbqdsdelete tf module
  - [autofixbqdsdelete - update subscription filter format](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/81)
    - To adapt with the change in monitor from outputting eventarc message to pubsub message with custom attributes
- metrics tf module **WARMING**
  - [add label content_type to log based metrics to improve observability](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/70)
    - Terraform will delete create the metrics, which may lead to errors 400 `That metric is still used in an alerting policy`
    - In this case, the workaround is to update from the console:
      - the metrics for which this error occurs
      - adding the following label to the metric:
        - label name: `content_type`
        - description: `content type, e.g. RESOURCES or IAM_POLICY`
        - filed name: `jsonPayload.contentType`
- deploy tf module
  - [Output RAM deploy service account name to ease OIDC integration](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/73)

### ram-v0-7-1

- execute
  - [accept caiExportAction empty assetTypes list meaning all assetTypes which enables to increase security of IAM policy exports avoid to miss new types](https://gitlab.com/realtime-asset-monitor/execute/-/issues/4) thanks to [Christian](https://github.com/cschroer) for the hint

### ram-v0-7-0

- setup
- `[Tune upload2gcs max concurrent requests to 12 and memory to 512MB](https://gitlab.com/realtime-asset-monitor/setup/-/issues/8)
- utilities
  - [Change WARNING "not_found_in_firestore" log entry to INFO to reduce logged entries in prod, as is a duplicate of convertfeed WARNING "Ancestor friendly name not found"](https://gitlab.com/realtime-asset-monitor/utilities/-/issues/36)
  - [add contentType to finish log entries to improve observability](https://gitlab.com/realtime-asset-monitor/utilities/-/issues/34)
  - Refactor `cai` types
  - [Add a rate_per_sec field to finish log entry to support throughput SLIs](https://gitlab.com/realtime-asset-monitor/utilities/-/issues/35)
- autofix
  - [Reduce finish message string cardinality by moving details to description string](https://gitlab.com/realtime-asset-monitor/autofix/-/issues/4)
- autofix
  - [change triggering event format from eventarc to pubsub raw to follow monitor output format change](https://gitlab.com/realtime-asset-monitor/autofix/-/issues/3)
- convertfeed
  - [remove duplicate warning log entries with message "Ancestor friendly name not found" and "not_found_in_firestore" by moving the last to INFO to reduce logged entries in prod](https://gitlab.com/realtime-asset-monitor/convertfeed/-/issues/12)
  - [drop eventarc format to manage pubsub metadata so that it ease subscription filtering](https://gitlab.com/realtime-asset-monitor/convertfeed/-/issues/11)
    - Add contentType to converted feed messages
    - Add to PubSub message the following attributes:
      - Always: assetType, contentType, messageType, microServiceName, origin
      - Optional: define in yaml config files which specific Key Values to add base on the Asset and content type
        - First use case: make configurable which feed messages have to be persisted to Firestore
        - Second use case: make configurable which feed messages have to be persisted to GCS
    - Manage retry with the pubsub subscription when publishing fail on a transient error
  - [remove duplicate warning log entries with message "Ancestor friendly name not found" and "not_found_in_firestore" by moving the last to INFO to reduce logged entries in prod](https://gitlab.com/realtime-asset-monitor/convertfeed/-/issues/12)
- fetchrules
  - [change triggering event format from eventarc to pubsub raw](https://gitlab.com/realtime-asset-monitor/fetchrules/-/issues/17)
    - So that it enables publish2fs to publish any kind of assettype . contentType to firestore
- monitor
  - [Replace eventarc format by pubsub in output messages to leverage filtering on attributes](https://gitlab.com/realtime-asset-monitor/monitor/-/issues/14)
- publish2fs
  - [Change triggering event format from EventArc to Pubsub raw](https://gitlab.com/realtime-asset-monitor/publish2fs/-/issues/1)
    - Add contentType to firestore document names when different from 'RESOURCE'
    - Add contentType to finish message to improve log filtering to ease troubleshooting
- splitexport
  - [Add to rate to finish log entries to support throughput SLIs](https://gitlab.com/realtime-asset-monitor/splitexport/-/issues/9)
- stream2bq
  - [change triggering event format from eventarc to pubsub raw](https://gitlab.com/realtime-asset-monitor/stream2bq/-/issues/14)
    - So that it enables publish2fs to publish any kind of assettype . contentType to firestore

- Update dependencies including
  - go 1.21.6
  - OPA v0.60.0
  - Google Artifact Registry vulnerability scanning detected CVE-2023-45285 which is [fixed](https://nvd.nist.gov/vuln/detail/CVE-2023-45285) using go 1.12.5 or above. This release is compiled using [1.21.6](https://gitlab.com/realtime-asset-monitor/_cicd/-/blob/main/templates/variables.yml?ref_type=heads#L24), so the issue should not be detected. I suspect the vulnerability scan to use go.mod file to determine the go version which is limited to 1.21, not enabling to see 1.12.6.

### terraform module v0.6.2

- Remove IAP brand and IAP client resources from the load balancer module:
  - IAM brand may already exists
    - It cannot be deleted (One per project)
    - If configure as ['External' it cannot be managed through the API](https://cloud.google.com/iap/docs/programmatic-oauth-clients#known_limitations)
- Replaced by:
  - You create or identify existing IAP Brand, on the project hosting RAM
    - For example using GCP console / API / OAuth.
    - Can be named 'Real-time Asset Monitor'
    - The account used to create the IAP brand must be owner of the group mentioned in the filed support_email
  - You create and IAP client on that Brand.
    - For example using GCP console / API / Credentials
    - Can be named 'ram-iap-client'
  - Store the IAP id and secret provided by the console at the previous step in two secrets secret Manager as:
    - `ram-iap-client-id`
    - `ram-iap-client-secret`
    - Use exactly these names as they are referenced as is in the terraform module to set IAP on the two backend services.
  - Bind the following IAM policy entry:
    1. Role 'Secret Manager Secret Accessor' `roles/secretmanager.secretAccessor`
    2. to the service account used to deploy the terraform module
    3. on the two above secrets.

### ram-v0-6-0

- console NEW!!
  - aka **RAM frontend**
  - optional see terraform module below
    - If you opt-in in terraform, think also the deploy tools you use to manage Cloud Run service image updates like
      - gcloud script [deploy_no_traffic.sh](updgrade/../upgrade/gcloud/deploy_no_traffic.sh) amd [update_traffic.sh](updgrade/../upgrade/gcloud/update_traffic.sh)
      - cloud build definition [deploy_no_traffic.yaml](upgrade/cloudbuild/deploy_no_traffic.yaml) and [update_traffic.yaml](upgrade/cloudbuild/update_traffic.yaml)
  - First user story: As an asset owner I want to see the compliance timeline of my asset
    - implemented as two pages
      - the assetList page to filter / select a given asset
      - the assetTimeline page to display a given asset compliance timeline
  - based on [flutter](flutter.dev) web, meaning is a SPA [Single Page App](https://en.wikipedia.org/wiki/Single-page_application)
  - Using [dio Dart package](https://pub.dev/packages/dio) as HTTP client
  - Using [openapi-generator](https://openapi-generator.tech/docs/generators/dart-dio) to generate dio code from the [Console BFF openapi3 API specifications](https://gitlab.com/realtime-asset-monitor/consolebff/-/blob/main/bff_openapi.yaml)
  - Hosting the SPA app as static content using [echo](https://echo.labstack.com/) Go web framework
  - Dockereizing the app using Cloud Native Buildpack as for other RAM microservices

- consolebff NEW!!
  - aka backend for frontend
  - optional see terraform module below
    - If you opt-in in terraform, think also the deploy tools you use to manage Cloud Run service image updates like
      - gcloud script [deploy_no_traffic.sh](updgrade/../upgrade/gcloud/deploy_no_traffic.sh) amd [update_traffic.sh](updgrade/../upgrade/gcloud/update_traffic.sh)
      - cloud build definition [deploy_no_traffic.yaml](upgrade/cloudbuild/deploy_no_traffic.yaml) and [update_traffic.yaml](upgrade/cloudbuild/update_traffic.yaml)
  - Specifying the [API](https://gitlab.com/realtime-asset-monitor/consolebff/-/blob/main/bff_openapi.yaml) using [OpenAPI v3](https://spec.openapis.org/oas/v3.0.0)
  - Exposing the related APIs for the first user story implemented in the frontend (console)
  - Based on [echo](https://echo.labstack.com/) go web framework
  - Using [DeepMap oapi-codegen](https://github.com/deepmap/oapi-codegen) to generate Go server code from the [Console BFF openapi3 API specifications](https://gitlab.com/realtime-asset-monitor/consolebff/-/blob/main/bff_openapi.yaml)
  - Dockereizing the app using Cloud Native Buildpack as for other RAM microservices
  
- stream2bq
  - [Record scheduledRootTriggeringTimestamp when stream to the compliance_status table to enable asset timeline visualizations](https://gitlab.com/realtime-asset-monitor/stream2bq/-/issues/13)

- Update all dependencies, including the following noticeable:
  - cloud native builder [gcr.io/buildpacks/builder:google-22](https://cloud.google.com/docs/buildpacks/builders) meaning Ubuntu 22 instead of 18
  - PACK cli v0.28.0
  - GO language 1.20.2
  - Flutter 3.7.7
  - OPA v0.50.1

### terraform module  v0.6.0

- [stream2bq module - Add scheduledRootTriggeringTimestamp to the compliance_status table to enable asset timeline visualizations](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/61)
- [new console module](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/tree/main/modules/console) and [new consolebff module](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/tree/main/modules/consolebff)
  - To deploy set the variable `deploy_console = true` from the root module
- [new loadbalancer module -Add GEHTTPSLBC Global External HTTPS Load Balancer Classic with IAP to expose console and consolebff](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/63)
  - Updated the [perquisites](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/blob/main/README.md) to use the terraform module
    - New APIs to activate
    - Ownership on the group which email is used as support email in the IAP brand
  - [RAM components graph](_docs/ram.svg) has been updated with the loadbalancer details
  - To deploy set the variables  from the root module

```terraform
  deploy_loadbalancer       = true
  dns_name                  = var.dns_name
  support_email             = var.support_email
```

### terraform module v0.5.4

- [Unused var pubsub_allowed_regions on feed submodule](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/57)

### terraform module v0.5.3

- [PubSup child module deployment may lead to Error 400: regions not allowed by the project's organization policy](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/55)

### ram-v0-5-1

- Update dependencies including OPA v0.46.1
- [rule examples](/test/rules/) in this repo
  - Thanks to [Fabio](https://gitlab.com/fabio.coatti1) for warning on [cast_set() to be soon obsolete in REGO](https://gitlab.com/realtime-asset-monitor/setup/-/issues/4)
  - Thanks to [Marc](https://gitlab.com/marc.fundenberger) for the hard work in [cleaning up the all the REGO rules code examples](https://gitlab.com/realtime-asset-monitor/setup/-/merge_requests/6)
    - **CALL TO ACTION** Please reuse the cleaned code or perform the same kind of change changes in you own rules
    - The Clean-up actions cover:
      - replace black/whitelist with deny/allowlist
      - replace deprecated function re_match() with regex.match()
      - replace deprecated function cast_set() with use of future keyword "in" (fixes #4)
      - update runtime lists
      - update location lists
      - and other small fixes
- fetchrules
  - [Cleanup embedded REGO code to move out obsolete functions](https://gitlab.com/realtime-asset-monitor/fetchrules/-/issues/15)
  - [ram cli option to redeploy fetchrule after uploading rules so that all instance restart with empty rule cache](https://gitlab.com/realtime-asset-monitor/fetchrules/-/issues/16)

### terraform module v0.5.2

- [Remove unused variables](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/53)

### terraform module v0.5.1 DEPRECATED

- terraform module
  - [Make GCS Lifecycles configurable for upload2gcs and executecaiexport](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/47)
  - [Create Lifecycles on GCS Buckets created in fetchrules and launch modules](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/48)
  - [Manage autofix tag key and value outside of RAM tf module](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/51)

### ram-v0-5-0 terraform module v0.5.0

Introducing a key new feature: **automated remediation**, named `autofix`, with a first use case `bqdsdelete`.  
Learn more in [`autofix bqdsdelete` documentation](/_docs/autofix.md)  
There is a new mandatory terraform variable : `autofix_org_ids`

- all microservices: update dependencies
- autofix
  - New microservice, [new git repo](https://gitlab.com/realtime-asset-monitor/autofix)
  - [Update ce type check with origin real-time](https://gitlab.com/realtime-asset-monitor/autofix/-/issues/2)
  - [Initialize new microservice autofix](https://gitlab.com/realtime-asset-monitor/autofix/-/issues/1)
- monitor
  - [Add origin to violation cloud event metadata to ease filtering for autoremediation](https://gitlab.com/realtime-asset-monitor/monitor/-/issues/13)
- terraform module
  - [Reduce autofix run cost by improving the trigger to filter out violations related to the scheduled exports](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/45)
  - [add hosting infra for autofix bqdsdelete](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/43)
  - [Fix missing deleted assets in last_complianceStatus](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/41))
  - [Update autofix, create autofixbqdsdelete as an optional module](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/39)
- setup
  - [Implement the specified BDD tests for autofix bqdsdelete feature](https://gitlab.com/realtime-asset-monitor/setup/-/issues/6)

### ram-v0-4-1 terraform module v0.4.1

- **WARNING**: [stream2bq fix violation.FeedMessage.Asset.UpdateTime not persisted to violation table](https://gitlab.com/realtime-asset-monitor/stream2bq/-/issues/12)
- [Use Behavior Driven Development to specify the first ram autoremediation use case: `autofix_bqdsdelete`](https://gitlab.com/realtime-asset-monitor/setup/-/issues/5)
- [monitor - improve violation cloud event metadata to ease filtering for autoremediation](https://gitlab.com/realtime-asset-monitor/monitor/-/issues/12)
- [stream2bq - update  violation cloud event type filtering as changed for autofix](https://gitlab.com/realtime-asset-monitor/stream2bq/-/issues/11)
- Terraform module
  - **NOTE** the tf plan will display no change between v0.4.1 and v0.4.0 as the new version introduces changes (autofix) that are not deployed by default
  - Cleanup (again) monitoring dashboard code for cleaner Terraform plan
  - Add submodule for `autofix`
    - Create tag key `autofix` with tag value `bqdsdelete` per organization
- go v1.19.1, and updated dependencies on update microservices, including function framework v1.6.0

### ram v0-4-0 terraform module v0-4-0 - DEPRECATED

Automate end to end acceptance tests:

- **Use this** to check the upgrade works well too: Instructions updated in [this README](test/README.md)
- Takes ~20 min so that is improve both velocity and reliability enable smaller frequent changes:
- Formalized feature using Gherkin language
- Automated using [godog](https://github.com/cucumber/godog) (cucumber's golang implementation)

Updated `action.json` template file:

- Lower gfsdeleteolddocs maAgeHours from 24 to 2
- Remove `k8s.io/Pod` asset type from the general IAM caiexport
- Add asset types: `bigquery.googleapis.com/Table`

Key improvement: get rid of zombie status

- Before: the `assetInventoryTimestamp` for the scheduled export flow was the GCS object timestamp leading the lingering status when the following sequence occurs:
  - t1 the asset is updated, realtime flow is delayed
  - t2 the asset is deleted, realtime flow is delayed
  - t3 an export is launched, the deleted asset is still in the export due to the delay in realtime
    - the compliance data are persisted with `assetInventoryTimestamp` as t3
  - t4 the realtime flow eventually delivers the asset deleted status that is persisted with `assetInventoryTimestamp` as t2
  - t5 the realtime flow eventually delivers the asset update status that is persisted with `assetInventoryTimestamp` as t1
  - Results
    - The system is consistent on the realtime flow, meaning the delete info wins as its `assetInventoryTimestamp` t2 is more recent thant the update with t1
    - BUT the scheduled export flow create a zombie status with `assetInventoryTimestamp` as t3 while it refers to the asset state before t1. It will stay for ever while the asset no longer exist
- After the improvement:
  - The export flow now leverage the field `updateTime` to hae an accurate timestamp when the asset was last changed
  - Results
    - The timeline of changes is consistent between real-time and scheduled export flows
    - The `assetInventoryTimestamp` is accurate to describe when an asset was last updated

ram all microservices:

- Upgrade to go v1.19
- Upgrade dependencies including Function Framework v1.6.1 (see [issue #150 on v1.6.0](https://github.com/GoogleCloudPlatform/functions-framework-go/issues/150))

monitor:

- Use `updateTime` for scheduled export flow to populate `assetInventoryTimestamp`
- Update dependencies including OPA v0.44.0

execute:

- Fix stepstack in record export to firestore

splitexport:

- Exported asset timestamp should be equal to the triggering cloud job timestamp (enable by the above fix on splitexport)

stream2bq:

- Use `updateTime` for scheduled export flow to populate `assetInventoryTimestamp`
- Improve observability so that is ease troubleshooting and support e2e tests by by persisting
  - `scheduledRootTriggeringID` in `assets`, `complianceStatus` and `violations` tables
  - `persistTimestamp` in `assets` table and last_assets view

terraform module

- Tunning SLO goals
- Split the scheduled latency slo in two: k8s pods asset and all other assets, so that slowness to k8s pod and speed for other assets could be observed, and update the related dashboards
- Updated default value for `bq_partition_expiration_ms` to be greater than the 30 days of the project pending deletion delay of 30 days
- Refactor the SQL views:
  - to surface new fields `scheduledRootTriggeringID` in all view and `persistTimestamp
  - to adapt the way to find last assets, last compliance status and active violation to the change on now using `updateTime` from the scheduled export flow
  - for more code clarity
- Fix [There must be only one last compliance status per rule per asset in the view last_compliancestatus](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/issues/30)

### terraform module v0.3.1

- Updated RAM Service Level Objectives: added Availability SLOs for each critical microservice, reworked the existing Latency SLOs
  - The SLO goal for microservice availability is set by default to 3 nines (99.9%) as is highest value supported by [the API](https://cloud.google.com/monitoring/api/ref_v3/rest/v3/services.serviceLevelObjectives) while four nines would be more adapted here from achievable observed data.
  - The Latency SLOs are conservative. Observe the achievable SLOs in your context and adapt, like:
    - Latency Real-time threshold may be lowered from 5.4min to 20sec, keeping the 95% goal
    - Latency Schedule threshold may be lowered from 31min to 15.5min, keeping the 99% goal
- Added Service Level Objectives for RAM main dependencies:
  - Cloud Asset Inventory latency in both real-time and export modes
  - Three most consumed Google APIs, with availability and latency SLOS for each:
    - BigQuery InsertAll
    - PubSub Publish
    - Firestore Commit
  - Look to changes in [main.tf](install/main.tf) and [variables.tf](install/variables.tf) and adapt yours accordingly
- Notification channels configurable to receive alerts related Service Level Indicators SLOs for RAM and for RAM main dependencies
- Each SLO has two alerts, one on fast burn rate, one on slow burn rate, as [recommended](https://cloud.google.com/stackdriver/docs/solutions/slo-monitoring/alerting-on-budget-burn-rate#burn-rates)
- Each SLO has an associated monitoring dashboard that leverages the [data insights provided by Cloud Monitoring SLO monitoring](https://cloud.google.com/stackdriver/docs/solutions/slo-monitoring/api/timeseries-selectors).

### ram-v0-3-0 terraform module v0.3.0

Warning to update to this version:

- Replace your output.tf by [this one](install/outputs.tf)
- Just run terraform two times in case the following error occurs:
  - `Error applying IAM policy for pubsub topic "projects/<project-id>/topics/alerting": Error setting IAM policy for pubsub topic "projects/<project-id>/topics/alerting": googleapi: Error 400: Service account service-<project-number>@gcp-sa-monitoring-notification.iam.gserviceaccount.com does not exist.`

Changes:

- `utilities`
  - feature: add assetType and ruleName to finish log entries
  - feature: add assetType to complianceStatus structure
  - feature: add latency_t2s meaning "trigger to start" to measure Cloud Asset Inventory CAI latency for both real-time feeds and exports
- `convertfeed fetchrules monitor stream2bq publish2fs upload2gcs` track assetType in finish log entry
- `monitor stream2bq` track ruleName in finish log entry
- Add `assetType` to `complianceStatus` in `monitor` `stream2bq`
- `setup` fix [Missing "launch" service in upgrade/gcloud/update_traffic.sh](https://gitlab.com/realtime-asset-monitor/setup/-/issues/3)
- `stream2bq` records `assetType` string field to `compliance_status` table. (requires RAM tf modules v0.3.0)
- go v1.18.4
- pack v0.27.0
- update all go libraries to latest, including OPA v0.42.2
- terraform modules
  - `stream2bq` add `assetType` string field to `compliance_status` table. Is nullable to allow the schema update on existing RAM setup. (requires RAM v0-3-0)
  - rename and reorganize the terraform modules
  - harmonize and simplify the outputs
  - add Service Monitoring / Service Level Objectives / Alerting / Dashboarding (requires RAM v0-3-0)
  - reduce dependency on terraform workspaces to manage environments [Christian contribution](https://github.com/cschroer)

### ram-v0-2-0

Key improvement:

- `splitexport` refactored way to read CAI exports.
  - Why?
    - Get rid of issue related to asset definition larger than the value set in `scanner_buffer_size_kilo_bytes` (default 256KB) leading to fail exports processing
  - How?
    - Replace loading the full export in memory by reading the export using a  [storage object range reader](https://cloud.google.com/storage/docs/samples/storage-download-byte-range), set by default to read by 10 MiB
  - Results
    1. splitexport can now read any size of asset. The parameter `scanner_buffer_size_kilo_bytes` is removed.
    2. No performance drop, even some improvement. Our peak test shows:
       - 10 minutes end to end to process ~1 million compliance statuses
    3. splitexport run cost are reduced ~x20. Our peak test shows:
       - Memory instance size reduced from 8 GiB to 1 GiB
       - CPU instance size reduced form 2 to 1
       - Observed multiconcurrency increase from 2 to 20
       - Observed decrease in number of parallel instances from 20 to 2
  - WARNING: related update to `splitexport` Cloud Run default settings in:
    - The [RAM Terraform module v0.2.0](https://github.com/BrunoReboul/terraform-google-realtime-asset-monitor/releases/tag/v0.2.0)
    - The upgrade templates:
      - [upgrade/cloudbuild/deploy_no_traffic.yaml](upgrade/cloudbuild/deploy_no_traffic.yaml)
      - [upgrade/gcloud/deploy_no_traffic.sh](upgrade/gcloud/deploy_no_traffic.sh)

Bug fix:

- `stream2bq`: fix persist violation: `feedMessage.asset.iamPolicy` field was always persisted as `null` string
- `fetchrules` / ramcli: fix corrupted csv file when using argument `-type`

Dependencies:

- Uses GO v1.18.2
- Update all dependencies to latest on 2022-05-18, including `monitor` to use OPA v0.40.0

### ram-v0-1-0

- RAMv2 Initial release

### ram-v0-0-1-rc03

- Update dependencies incl OPA v0.37.0
- Fix `monitor` concurrency issue by passing documents using `input` node instead of loading documents using `data` node
- Adapt `audit.rego` to get documents from `input` node instead of `data` node
- Clean and lean AssetsJSONDocument no more needed in `utilities` cai types assetRule
- Terraform module v0.0.4
  - Fix stream2bq module: leverage lastEvaluation timestamp correctly in last compliance status view

### ram-v0-0-1-rc02

- Updated dependencies

### ran-v0-0-1-dc01

- First release candidate
- Covers core microservices only: convertfeed, ftechrules, monitor, stream2bq
