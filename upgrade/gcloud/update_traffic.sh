#!/usr/bin/env bash

# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the 'License');
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an 'AS IS' BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if [[ -z "$1" ]]; then
    TAG=latest
else
    TAG=$1
fi
echo TAG $TAG
echo PROJECT_ID $PROJECT_ID ENVIRONMENT $ENVIRONMENT

if [[ -z "$2" ]]; then
    TRAFFIC_PERCENT=100
else
    TRAFFIC_PERCENT=$2
fi
echo TRAFFIC_PERCENT $TRAFFIC_PERCENT

REGION=europe-west1

gcloud config set project $PROJECT_ID

function update_traffic (){
    echo $SERVICE_NAME $TAG $TRAFFIC_PERCENT
    gcloud run services update-traffic ${SERVICE_NAME} \
        --to-tags=${TAG}=${TRAFFIC_PERCENT} \
        --region=${REGION} \
        --platform=managed \
        --project=${PROJECT_ID}
}

for SERVICE_NAME in convertfeed executecaiexport executegfsdeleteolddocs fetchrules launch monitor publish2fs splitexport stream2bq upload2gcs; do
    update_traffic
done